/*
 Navicat Premium Data Transfer

 Source Server         : postgre-local
 Source Server Type    : PostgreSQL
 Source Server Version : 140004
 Source Host           : 127.0.0.1:5432
 Source Catalog        : kai-timeline
 Source Schema         : vnd_mgm

 Target Server Type    : PostgreSQL
 Target Server Version : 140004
 File Encoding         : 65001

 Date: 31/08/2023 07:46:52
*/


-- ----------------------------
-- Sequence structure for failed_jobs_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."failed_jobs_id_seq";
CREATE SEQUENCE "vnd_mgm"."failed_jobs_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."migrations_id_seq";
CREATE SEQUENCE "vnd_mgm"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for mst_pengadaan_vms_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."mst_pengadaan_vms_id_seq";
CREATE SEQUENCE "vnd_mgm"."mst_pengadaan_vms_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for personal_access_tokens_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."personal_access_tokens_id_seq";
CREATE SEQUENCE "vnd_mgm"."personal_access_tokens_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for trx_master_business_qualifications_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."trx_master_business_qualifications_id_seq";
CREATE SEQUENCE "vnd_mgm"."trx_master_business_qualifications_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "vnd_mgm"."users_id_seq";
CREATE SEQUENCE "vnd_mgm"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."failed_jobs";
CREATE TABLE "vnd_mgm"."failed_jobs" (
  "id" int8 NOT NULL DEFAULT nextval('"vnd_mgm".failed_jobs_id_seq'::regclass),
  "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "connection" text COLLATE "pg_catalog"."default" NOT NULL,
  "queue" text COLLATE "pg_catalog"."default" NOT NULL,
  "payload" text COLLATE "pg_catalog"."default" NOT NULL,
  "exception" text COLLATE "pg_catalog"."default" NOT NULL,
  "failed_at" timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."migrations";
CREATE TABLE "vnd_mgm"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('"vnd_mgm".migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "vnd_mgm"."migrations" VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "vnd_mgm"."migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO "vnd_mgm"."migrations" VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO "vnd_mgm"."migrations" VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 2);
INSERT INTO "vnd_mgm"."migrations" VALUES (5, '2023_08_07_004832_add_new_column_to_pic_vnd_table', 2);
INSERT INTO "vnd_mgm"."migrations" VALUES (6, '2023_08_07_010623_create_trx_kepemilikan_vnd_table', 3);
INSERT INTO "vnd_mgm"."migrations" VALUES (7, '2023_08_07_010931_create_trx_sign_contract_table', 4);
INSERT INTO "vnd_mgm"."migrations" VALUES (8, '2023_08_07_011040_create_trx_akta_vnd_table', 4);
INSERT INTO "vnd_mgm"."migrations" VALUES (9, '2023_08_07_012144_delete_trx_pnd_pic_contacy_table', 5);
INSERT INTO "vnd_mgm"."migrations" VALUES (10, '2023_08_12_232207_create_table_trx_afiliasi', 6);
INSERT INTO "vnd_mgm"."migrations" VALUES (11, '2023_08_12_232425_create_table_trx_kompetensi_ref_vnd', 7);
INSERT INTO "vnd_mgm"."migrations" VALUES (12, '2023_08_12_232618_create_table_trx_principal', 7);
INSERT INTO "vnd_mgm"."migrations" VALUES (13, '2023_08_12_233020_create_table_trx_doc_pendukung', 7);
INSERT INTO "vnd_mgm"."migrations" VALUES (14, '2023_08_12_233355_create_table_trx_finance_legal_admin', 8);
INSERT INTO "vnd_mgm"."migrations" VALUES (15, '2023_08_14_005711_create_trx_afiliasi_table', 9);
INSERT INTO "vnd_mgm"."migrations" VALUES (16, '2023_08_14_005844_create_trx_kompetensi_ref_vnd_table', 9);
INSERT INTO "vnd_mgm"."migrations" VALUES (17, '2023_08_14_005923_create_trx_principal_table', 9);
INSERT INTO "vnd_mgm"."migrations" VALUES (18, '2023_08_14_005951_create_trx_doc_pendukung_table', 9);
INSERT INTO "vnd_mgm"."migrations" VALUES (19, '2023_08_14_010026_create_trx_finance_legal_admin_table', 9);
INSERT INTO "vnd_mgm"."migrations" VALUES (20, '2023_08_14_021859_create_trx_afiliasi_table', 10);
INSERT INTO "vnd_mgm"."migrations" VALUES (21, '2023_08_14_022028_create_trx_finance_legal_admin_table', 10);
INSERT INTO "vnd_mgm"."migrations" VALUES (22, '2023_08_14_022132_create_trx_doc_pendukung_table', 10);
INSERT INTO "vnd_mgm"."migrations" VALUES (23, '2023_08_14_022315_create_trx_principal_table', 10);
INSERT INTO "vnd_mgm"."migrations" VALUES (24, '2023_08_14_022416_create_trx_kompetensi_ref_vnd_table', 10);
INSERT INTO "vnd_mgm"."migrations" VALUES (25, '2023_08_19_061753_create_trx_legal_finance_table', 11);
INSERT INTO "vnd_mgm"."migrations" VALUES (26, '2023_08_22_013245_create_mst_pengadaan_vms_table', 12);
INSERT INTO "vnd_mgm"."migrations" VALUES (27, '2023_08_22_050127_create_trx_master_business_qualifications', 13);
INSERT INTO "vnd_mgm"."migrations" VALUES (28, '2023_08_24_010726_create_trx_vnd_general_info_table', 14);

-- ----------------------------
-- Table structure for mst_pengadaan_vms
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."mst_pengadaan_vms";
CREATE TABLE "vnd_mgm"."mst_pengadaan_vms" (
  "id" int8 NOT NULL DEFAULT nextval('"vnd_mgm".mst_pengadaan_vms_id_seq'::regclass),
  "pengadaan_name" text COLLATE "pg_catalog"."default",
  "pengadaan_date" date,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of mst_pengadaan_vms
-- ----------------------------
INSERT INTO "vnd_mgm"."mst_pengadaan_vms" VALUES (1, 'KEBUTUHAN PENGADAAN IT', '2023-08-24', '2023-08-24 19:52:01', '2023-08-24 19:52:04');
INSERT INTO "vnd_mgm"."mst_pengadaan_vms" VALUES (2, 'PENGADAAN GERBONG KERETA ', '2023-08-21', NULL, NULL);
INSERT INTO "vnd_mgm"."mst_pengadaan_vms" VALUES (3, 'PENGADAAN KURSI KERETA', '2023-08-08', '2023-08-24 12:54:31', '2023-08-24 12:54:31');
INSERT INTO "vnd_mgm"."mst_pengadaan_vms" VALUES (4, 'PENGADAAN JASA CLEANING SERVICE', '2023-08-15', '2023-08-24 12:54:54', '2023-08-24 12:54:54');

-- ----------------------------
-- Table structure for mst_sub_vnd_type
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."mst_sub_vnd_type";
CREATE TABLE "vnd_mgm"."mst_sub_vnd_type" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "vendor_type_id" uuid,
  "sub_type_vendor" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of mst_sub_vnd_type
-- ----------------------------
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('955f342e-540d-454b-80c8-e8da9e69440a', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'PT', '2023-07-12 21:14:15', '2023-07-12 21:14:19');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('ce90f5dc-c12b-4d98-af62-4e3467a8eb8a', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'CV', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('9da7bfd6-5530-448c-b93d-5120f61f1aaf', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'Koperasi', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('7fbdfe61-9474-426e-b54e-ee90c7ee2be3', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'BUT (Badan Usaha Terbatas)', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('a2ad13bf-215b-4cb2-8ee5-db7f948e9469', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'Sekolah / Universitas', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('5a629af9-4aeb-4326-80d1-07ebda8e35d1', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'Yayasan', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('5f2b90b2-2517-4f7c-ab6c-5e15c6648b99', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'Firma Hukum', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('562f2285-767a-4261-8353-ae2160015dda', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'KJPP (Kantor Jasa Penilai Publik)', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('aa649947-2f8f-40ae-956f-19951dce1459', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'KAP (Kantor Akutan Publik)', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('ba9729e3-a7de-4a53-950f-a41fd3d1e6d2', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'BUMDES (Badan Usaha Milik Desa)', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('27afb7da-2e8c-4f38-a3cb-fce64559c2e8', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'Lainnya', '2023-07-12 21:14:15', '2023-07-12 21:14:15');
INSERT INTO "vnd_mgm"."mst_sub_vnd_type" VALUES ('38b82070-99aa-416d-b2f5-b10462af1626', '9580e7bc-6841-467b-8257-7aec651ae7f9', 'PT. Perorangan', '2023-07-12 21:14:15', '2023-07-12 21:14:15');

-- ----------------------------
-- Table structure for mst_vnd_type
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."mst_vnd_type";
CREATE TABLE "vnd_mgm"."mst_vnd_type" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "vendor_type" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of mst_vnd_type
-- ----------------------------
INSERT INTO "vnd_mgm"."mst_vnd_type" VALUES ('9580e7bc-6841-467b-8257-7aec651ae7f9', 'Non-Perorangan', '2023-07-12 21:05:17', '2023-07-12 21:05:22');
INSERT INTO "vnd_mgm"."mst_vnd_type" VALUES ('f49bae3c-9e8f-4730-be8e-00629950ebb2', 'Perorangan', '2023-07-12 21:06:54', '2023-07-12 21:06:57');
INSERT INTO "vnd_mgm"."mst_vnd_type" VALUES ('0e083d4b-6a11-4cc0-9e35-ab1fce9a4669', 'Luar Negeri', '2023-07-12 21:07:37', '2023-07-12 21:07:40');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."password_resets";
CREATE TABLE "vnd_mgm"."password_resets" (
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0)
)
;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."personal_access_tokens";
CREATE TABLE "vnd_mgm"."personal_access_tokens" (
  "id" int8 NOT NULL DEFAULT nextval('"vnd_mgm".personal_access_tokens_id_seq'::regclass),
  "tokenable_type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "tokenable_id" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "abilities" text COLLATE "pg_catalog"."default",
  "last_used_at" timestamp(0),
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (1, 'App\Models\User', 5, 'myAppToken', '1302c94412c13000ab788f0f74c3b2f3ff6b13899a3a2fc4f3f454839c84209e', '["*"]', NULL, '2023-08-06 15:36:22', '2023-08-06 15:36:22');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (15, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'e0a5fa64fd31c41f1115bd44135927516fd68ee28c263e43269c315f7e38527f', '["*"]', '2023-08-09 14:07:34', '2023-08-09 14:06:51', '2023-08-09 14:07:34');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (28, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '70215974f8fdd52bf6b91d8e5081e2f08136435d7eeaf30148eca75150cf230a', '["*"]', '2023-08-10 08:43:07', '2023-08-10 08:42:40', '2023-08-10 08:43:07');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (29, 'App\Models\User', 9, 'test@gmail.com', '8840b4050a35c2b8ee87363499ef45b62853fa3951bad408de99bbd879494ccb', '["*"]', NULL, '2023-08-10 09:25:04', '2023-08-10 09:25:04');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (16, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'f5766a2c149092d5fe8effeb5b193d77557a13c495d8d28618b9699a5c9dc871', '["*"]', '2023-08-09 14:08:15', '2023-08-09 14:08:05', '2023-08-09 14:08:15');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (2, 'App\Models\User', 5, 'myAppToken', 'eac022474c6933f87d92125dd3f1e2ec38a41b240c74fbf3e22651765760f434', '["*"]', '2023-08-06 15:38:19', '2023-08-06 15:36:37', '2023-08-06 15:38:19');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (3, 'App\Models\User', 6, 'ali.dev.jkt@gmail.com', 'e801c817a99e9918748af5a059aa008a09e7445a055400e443cdddc19a67e9b8', '["*"]', NULL, '2023-08-08 02:49:02', '2023-08-08 02:49:02');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (23, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '89fae602efdd2a6d5bd8980d50d43b9b0983a0b5bb1ff9ae29bfd1f34d56a31f', '["*"]', '2023-08-09 14:34:43', '2023-08-09 14:33:35', '2023-08-09 14:34:43');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (17, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '5b1d9ef1cfac7fc99ab9b13ae65890eb64e0cb2b8e2865790443ce9ce166a7e6', '["*"]', '2023-08-09 14:08:42', '2023-08-09 14:08:29', '2023-08-09 14:08:42');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (18, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '519d614366f382e34e04f439961ac4d3a744e7df2a7b54fb41a59af84fd2f934', '["*"]', '2023-08-09 14:08:54', '2023-08-09 14:08:54', '2023-08-09 14:08:54');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (5, 'App\Models\User', 6, 'ali.dev.jkt@gmail.com', 'c8f8a21089e1b61f05ecab848b3514d818cc99dcc061b47e738ad85f517c7975', '["*"]', '2023-08-08 04:11:25', '2023-08-08 03:54:17', '2023-08-08 04:11:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (21, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'e297d53c4aa2234ea3d1ea683255dc2c70af2998311f1979d5b9f1ca07e1fc0c', '["*"]', '2023-08-09 14:34:51', '2023-08-09 14:15:15', '2023-08-09 14:34:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (6, 'App\Models\User', 6, 'ali.dev.jkt@gmail.com', '08f3d3bf45df9c23d9ddb9a17d20e7d53e6c2b94b884a44eea2d6aa3c4859a9d', '["*"]', '2023-08-08 08:49:28', '2023-08-08 08:48:28', '2023-08-08 08:49:28');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (7, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '21f4f75e2ac820aebad5d6cfc2b5c2910598afb0fdb78cdd8ea63d047b9578dc', '["*"]', NULL, '2023-08-09 13:50:06', '2023-08-09 13:50:06');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (8, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'd549dd690f38c6699431b9e324846ace634af792b1db4a7586078cb27143c017', '["*"]', '2023-08-09 13:50:17', '2023-08-09 13:50:16', '2023-08-09 13:50:17');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (9, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '127453bb9d28489c07a01552c2b5352c7a94cfc35f96b2b446873900c51a8b57', '["*"]', '2023-08-09 13:52:59', '2023-08-09 13:52:59', '2023-08-09 13:52:59');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (10, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'a33f82863102ccb49a6e898e8babce2c2139c05ef4bffad679cb0babb4d29d5a', '["*"]', '2023-08-09 13:54:28', '2023-08-09 13:54:28', '2023-08-09 13:54:28');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (11, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'd1e80c34630b79b83da94ce14f4cea8ba4214cec39b808b33c3c8764d9671db6', '["*"]', '2023-08-09 13:56:31', '2023-08-09 13:56:30', '2023-08-09 13:56:31');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (19, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '6b8be97b5484beb7d9b800d9b190a0fdde1b862a6ecdbe7fd502cb92c34dd0f9', '["*"]', '2023-08-09 14:13:26', '2023-08-09 14:10:41', '2023-08-09 14:13:26');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (12, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'f5612676160f691ef5c9bcea7422b0889d913b68e8ea95e54f0dd9ce3e1c8d82', '["*"]', '2023-08-09 14:04:53', '2023-08-09 14:00:07', '2023-08-09 14:04:53');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (32, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '590d0d38adbfc676947424f8de7d108fde74df97502e581a02d491734f38aca4', '["*"]', '2023-08-10 10:51:04', '2023-08-10 10:31:10', '2023-08-10 10:51:04');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (4, 'App\Models\User', 6, 'ali.dev.jkt@gmail.com', '2a1f3ccd65ccd3ea2fcd3af715e2c7a37b6deb4b1cc512373bd268e1dbc53784', '["*"]', '2023-08-08 03:33:29', '2023-08-08 02:49:19', '2023-08-08 03:33:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (13, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '8ad69fa1f6ac7f229fb720b5867e3f475083b7935dea971cf7e46ec4f3c71c83', '["*"]', '2023-08-09 14:05:26', '2023-08-09 14:05:03', '2023-08-09 14:05:26');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (20, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'f477dcb7963037ef6d22ff46a68e974af9c3077b72b4f7541077817e5b0e060d', '["*"]', '2023-08-09 14:13:58', '2023-08-09 14:13:38', '2023-08-09 14:13:58');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (14, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '36915505b5f8aaed299c1511a80d02a7b8067acd03359d7896a293f9519f3920', '["*"]', '2023-08-09 14:05:39', '2023-08-09 14:05:39', '2023-08-09 14:05:39');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (24, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '48c236ea1f2faf144ec655c7ebf9d60bf47a224529ca4308d9eda41c2ac990c2', '["*"]', '2023-08-09 14:35:35', '2023-08-09 14:34:55', '2023-08-09 14:35:35');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (22, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'a2e5dbc187ecf05c9bc246a483b6f03704084d707a16db0efd28f0fb59d53ec4', '["*"]', '2023-08-09 14:33:20', '2023-08-09 14:32:45', '2023-08-09 14:33:20');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (25, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'e3e1f47cb2e13f86a31f72f824772f4d40e78d05f956413303680cf8f53a1c16', '["*"]', '2023-08-10 03:36:09', '2023-08-10 03:29:09', '2023-08-10 03:36:09');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (26, 'App\Models\User', 8, 'alihamzahar@gmail.com', '87715680a30b7e0054bd5df80e7b0fb0ca3523c3697b1eb980d1a08721fead3f', '["*"]', NULL, '2023-08-10 03:36:45', '2023-08-10 03:36:45');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (39, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '4e78f60f152255342299009848aa4db1cfc727a84d08e75b300335712ab4b15f', '["*"]', '2023-08-10 14:57:41', '2023-08-10 14:34:26', '2023-08-10 14:57:41');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (27, 'App\Models\User', 8, 'alihamzahar@gmail.com', '74b902aa33975c95d5f499b64d1f4ce187ab6fca9de85f3f1583f15f99ea02c2', '["*"]', '2023-08-10 03:37:38', '2023-08-10 03:36:53', '2023-08-10 03:37:38');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (30, 'App\Models\User', 9, 'test@gmail.com', '1c51b528316d137a5878a94951be6b49244fe381e0d9beed79f25197235fd078', '["*"]', '2023-08-10 09:36:03', '2023-08-10 09:25:20', '2023-08-10 09:36:03');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (34, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'dde7292cfb4630678200813722ce2a1f59c4e42a9067f5c9ba3b021aa6792360', '["*"]', '2023-08-10 12:03:02', '2023-08-10 11:49:19', '2023-08-10 12:03:02');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (35, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '22e64b2fbb8e85d93ca8cb273c71baa1d23b17e4946c58476174ff4f6fc64f0a', '["*"]', NULL, '2023-08-10 12:41:20', '2023-08-10 12:41:20');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (33, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'f74bffddf9ddfafc088bca2c615177e651cbef3ac55e78c938b8832bbd99eedf', '["*"]', '2023-08-10 11:46:37', '2023-08-10 11:18:51', '2023-08-10 11:46:37');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (31, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'b0c55493ee035bec013bb17f2c61214bd35d5252728b4dbf867123216e35a3ee', '["*"]', '2023-08-10 10:27:19', '2023-08-10 09:58:53', '2023-08-10 10:27:19');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (36, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '5720f834d45684686a6f397aab43e24284af77d2f65da8770baa1b591338a175', '["*"]', NULL, '2023-08-10 13:17:07', '2023-08-10 13:17:07');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (37, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '1963970bea60e2569646590a6e27336202fd2198c9d7ae833d911fc4f78afc58', '["*"]', NULL, '2023-08-10 13:18:29', '2023-08-10 13:18:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (38, 'App\Models\User', 9, 'test@gmail.com', '9b8b736644ada45934be99965a8e46a997009e275cc61bdcf6e6851f414d098f', '["*"]', NULL, '2023-08-10 14:01:20', '2023-08-10 14:01:20');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (72, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '88887a20951729508c57cb5a573d5e3a706cb10c701a5a051f0f04dbff11b9be', '["*"]', '2023-08-16 11:15:48', '2023-08-16 11:15:14', '2023-08-16 11:15:48');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (73, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'fe032e34d1e26674e39037194fe10ab0ff5bdf787930ce4da1734036703a407d', '["*"]', NULL, '2023-08-16 11:21:29', '2023-08-16 11:21:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (48, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'e5902d6757c33750bae3495e89efe0e676300e23cf0a57b2e812f496016e7b94', '["*"]', '2023-08-11 02:22:57', '2023-08-11 01:58:59', '2023-08-11 02:22:57');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (55, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '716ba9537a9b02a693707d41cf1df455091b07e9fbacc2b4068be3545eb29e76', '["*"]', '2023-08-11 07:33:29', '2023-08-11 07:26:17', '2023-08-11 07:33:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (49, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '14c70e12c1a8065980dfa499ab1e2ef7c24a6be123f8728e1c67db3c2a3b7435', '["*"]', '2023-08-11 02:39:45', '2023-08-11 02:33:46', '2023-08-11 02:39:45');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (40, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'ac225ff9927b704687d4078e14634cc59a4720b197e881c44676fe32c866e147', '["*"]', '2023-08-10 15:00:27', '2023-08-10 14:45:57', '2023-08-10 15:00:27');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (41, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '2f58fba9abb2a8d9b7c436f07b341e37eb8ab3f0750f40f23a30f29a34cd70cb', '["*"]', NULL, '2023-08-10 15:20:39', '2023-08-10 15:20:39');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (51, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '0c7bedfa3a070c9bede62e01e24cbda26062d32092dd42390f60ea98b300acd9', '["*"]', '2023-08-11 03:45:55', '2023-08-11 03:27:05', '2023-08-11 03:45:55');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (42, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '8b7ebd0337ef3d8678e3471cf07b303104ba583d9e381a871f37ff75bd3f4111', '["*"]', '2023-08-10 15:31:41', '2023-08-10 15:31:02', '2023-08-10 15:31:41');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (50, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '38c16dc46d3e071f182d1a7eaea3c9f91a0af8308e50d6a71f13e648a85232de', '["*"]', '2023-08-11 03:15:33', '2023-08-11 03:07:06', '2023-08-11 03:15:33');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (52, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '2c8d5bc2ecb8bbdbf5187786eb213025e11ffcabf70d1a25662759ab6f8abc3c', '["*"]', '2023-08-11 03:54:32', '2023-08-11 03:49:29', '2023-08-11 03:54:32');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (43, 'App\Models\User', 9, 'test@gmail.com', '4b6bb0517df8144136115e76234c93a66c410368631ba456f6966fd9dcc83856', '["*"]', '2023-08-10 15:41:31', '2023-08-10 15:36:54', '2023-08-10 15:41:31');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (44, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '419b9e0cf4e10bd25031a64dfac491e589f561303ddbef71735f591760683a77', '["*"]', NULL, '2023-08-10 23:15:08', '2023-08-10 23:15:08');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (45, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'd60494cc15542e6e13a0eb2f5acda8351439a8088f6ba98f26c97a1e8f1ae0ff', '["*"]', NULL, '2023-08-10 23:45:55', '2023-08-10 23:45:55');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (46, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '89ed4a1709c50aac96182e9c49b47891ed998edf5f9f1d2ce043f379373f4165', '["*"]', NULL, '2023-08-10 23:49:09', '2023-08-10 23:49:09');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (56, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '9b821f9a15fef0ec44eebe448dc98ab3c55f36d9e242e128fbfaf82cf3d0c9ed', '["*"]', '2023-08-11 08:15:44', '2023-08-11 08:15:35', '2023-08-11 08:15:44');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (57, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'cecb5cc08d62365e65f3a7e3b50f562ed64ba3d2c248e4e39a7161e74a90b770', '["*"]', '2023-08-11 08:22:13', '2023-08-11 08:22:04', '2023-08-11 08:22:13');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (63, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '1f4b373438efe69240a75b2da72eff44462e5f95a72ffa9b0f0e3ca07b4ecd29', '["*"]', '2023-08-14 05:27:25', '2023-08-14 05:27:11', '2023-08-14 05:27:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (59, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '5ff0d838d36cded64c63765f7c51702f1de8133cc7fef936e22a53de2da3ae67', '["*"]', '2023-08-11 10:29:53', '2023-08-11 10:16:16', '2023-08-11 10:29:53');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (47, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'a1a8be6117627e2da1c9b53f4e8cb8b3ecc1a9bb2e2f58b6b99387e48b761115', '["*"]', '2023-08-11 01:34:31', '2023-08-11 01:15:45', '2023-08-11 01:34:31');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (76, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '5b3133cac278f0a3f4927d943b1ad7e9a86242363d1a6537fd692a8a6cf29161', '["*"]', '2023-08-16 12:25:33', '2023-08-16 12:24:24', '2023-08-16 12:25:33');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (58, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '66e22263f1e00d3c2744a260cea74a30806cb39eaafcfe98ae1d7c444f4a7ba3', '["*"]', '2023-08-11 08:55:58', '2023-08-11 08:54:55', '2023-08-11 08:55:58');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (69, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '3a31ea9175159794aa4d449f21107f4e06041a97f36064d6a331f2818b7c450b', '["*"]', '2023-08-16 04:57:02', '2023-08-16 04:47:43', '2023-08-16 04:57:02');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (70, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '3b1d9b2cf61855d5561d0f6b1f0a5b30ef0774f6da51c7f26cc03d1efa7dfec4', '["*"]', NULL, '2023-08-16 05:44:15', '2023-08-16 05:44:15');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (53, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '24c422ec4b97d4baa1b4e9542aeba0b417e3e0f22cbfb30509d3d7ed5960c34c', '["*"]', '2023-08-11 06:59:47', '2023-08-11 06:39:54', '2023-08-11 06:59:47');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (54, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '867f93b8a8b43096bac50884090b713ab355b76b4d903e8bbfa07eb769f3680f', '["*"]', NULL, '2023-08-11 07:26:16', '2023-08-11 07:26:16');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (71, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'd610bfa4a2c0435aacc2c1272dc21587496230f68922e7b8e2280638ef933ac3', '["*"]', NULL, '2023-08-16 05:51:25', '2023-08-16 05:51:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (68, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '62f53e739e6562aaff310e376afa25b60c6c468212a5d6cbb1e5061c591edd74', '["*"]', '2023-08-15 13:26:15', '2023-08-15 13:19:55', '2023-08-15 13:26:15');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (60, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '4ba91adce8bf3268ae4152b9e086af8e22c386b400ada2e1903c86a3afa378b2', '["*"]', '2023-08-12 23:21:00', '2023-08-12 23:20:26', '2023-08-12 23:21:00');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (61, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'b36b164c606ff24255f0243087f499cd07083c443834cfc558f408520759bdc8', '["*"]', NULL, '2023-08-14 01:24:17', '2023-08-14 01:24:17');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (74, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '4d44694244ee4a64e65dd8c0fad30586a306bb89aebec3ce1bf0a16ffcb0a27c', '["*"]', '2023-08-16 11:29:12', '2023-08-16 11:23:15', '2023-08-16 11:29:12');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (62, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '6ddc7d196fa30b764dff868da69a9c08e9bc6aa38b9a641b30973c8896918f99', '["*"]', '2023-08-14 02:47:42', '2023-08-14 02:47:32', '2023-08-14 02:47:42');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (65, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '4e185986468cf61d73ada91e89a89bb2ab2cde030bfdb95679621e0fdbcfac36', '["*"]', '2023-08-15 13:19:21', '2023-08-15 12:29:36', '2023-08-15 13:19:21');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (67, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '5de5ab023974c820319ac4bc44e80d45234fa549fd2ee496aa6df0b837a2afa5', '["*"]', '2023-08-15 13:19:30', '2023-08-15 12:45:18', '2023-08-15 13:19:30');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (64, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '63d830f13d75091ff3ded7969936834d8fc0c8dece132abf41c3a55a9e1e8577', '["*"]', '2023-08-14 09:33:51', '2023-08-14 09:32:54', '2023-08-14 09:33:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (75, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '6f1693b7931ece3e4292bfb64673566fc1b22a4aeda868ae3cab047a713bdb53', '["*"]', NULL, '2023-08-16 12:23:20', '2023-08-16 12:23:20');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (66, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '4f5a9f8b45301a2db032940e90cd4e6787bde28ec363155c74c9c32bd69461ff', '["*"]', '2023-08-15 12:39:58', '2023-08-15 12:36:44', '2023-08-15 12:39:58');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (77, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '99f3fd6a173b0df8701f2fe4d2c03da21b49b0433ba40ed21339eaf759be211c', '["*"]', '2023-08-16 13:17:33', '2023-08-16 13:13:58', '2023-08-16 13:17:33');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (81, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'c838f4f1d50b3ac8f3cb6d61eb3e6cb66413b159df87f31b66273071cd17a20f', '["*"]', '2023-08-18 13:45:51', '2023-08-18 13:45:25', '2023-08-18 13:45:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (78, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '7302950812b459cdeb1f85f3643209ed4e020dc7b61efb09a79ca10b8c01717d', '["*"]', '2023-08-16 13:33:56', '2023-08-16 13:30:01', '2023-08-16 13:33:56');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (80, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'cf59827e656b39372a2c9e1973800d6d70564119eeb9ff6a369d24e4d2993a31', '["*"]', '2023-08-18 05:56:08', '2023-08-18 05:51:55', '2023-08-18 05:56:08');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (79, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '22c97945976957ddb04e35f522a409116920ae92339cdebf9cb4add13ed11a77', '["*"]', NULL, '2023-08-18 02:33:51', '2023-08-18 02:33:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (82, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'df687cc1cc37d1ac8895bafed18e4a07e6c0343cbd8c054facc52e67687f6660', '["*"]', '2023-08-21 06:12:32', '2023-08-21 05:57:24', '2023-08-21 06:12:32');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (106, 'App\Models\User', 11, 'alihamzah@gmail.com', 'dfffd3822cdce00b508f3c5611c0bd79758c613c379549508f0c91d2e4373014', '["*"]', NULL, '2023-08-28 01:52:14', '2023-08-28 01:52:14');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (94, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'c3b5d0d4309e0facf89b456c5b253abf7da90c6f9c9556d4b74b051b813f04ab', '["*"]', '2023-08-21 12:21:31', '2023-08-21 12:21:17', '2023-08-21 12:21:31');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (110, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'ef4cc2ef6a8f541e8dc39566924c209d5b63f9843bfc1fabed6018c4c3357c5e', '["*"]', NULL, '2023-08-29 05:51:17', '2023-08-29 05:51:17');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (85, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'ad4df5141a26d66b7b64c27dbcbd58de5d6e82b1e3d47bab87eb660149173220', '["*"]', '2023-08-21 06:50:08', '2023-08-21 06:48:53', '2023-08-21 06:50:08');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (86, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '5877bd7e1180495895f1b8762822dc3423c1267593c7241af32766e101e54721', '["*"]', NULL, '2023-08-21 07:33:41', '2023-08-21 07:33:41');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (87, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '21f66d6f119e7617320697fd72041e6bbfc7505d6e41e23b5ae90919add8ded4', '["*"]', NULL, '2023-08-21 07:35:33', '2023-08-21 07:35:33');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (83, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '53c784e625dde9a4b3a2cd7df1b6c2ac4672dd32a72ec81ad37b2a11798d21a8', '["*"]', '2023-08-21 06:12:31', '2023-08-21 06:11:59', '2023-08-21 06:12:31');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (113, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '1f3528df200a3993f339c3f28f783b4286c109ebf01305e2be61e5359a542f71', '["*"]', '2023-08-29 07:19:49', '2023-08-29 07:04:47', '2023-08-29 07:19:49');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (104, 'App\Models\User', 11, 'alihamzah@gmail.com', 'fe5bca1397b758e9c73fa1e1ef3e2f1a3e9a51ba3770c58c3231015fa15193ef', '["*"]', NULL, '2023-08-28 01:50:04', '2023-08-28 01:50:04');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (97, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '472839f1096b1d57e8d45fa0277cbfc8df02ffc082ecff86426af8cb191da91b', '["*"]', '2023-08-24 09:37:14', '2023-08-24 09:33:22', '2023-08-24 09:37:14');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (102, 'App\Models\User', 11, 'alihamzah@gmail.com', '6d729a375a7c4cf89408b75498aa717103f9afb504dba1572207d55da7b2144c', '["*"]', NULL, '2023-08-28 01:49:10', '2023-08-28 01:49:10');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (103, 'App\Models\User', 11, 'alihamzah@gmail.com', 'd6d917ba0336b966a958e066de17fd57adc7c412ee17abfd8c6fdce0b8b36b28', '["*"]', '2023-08-28 01:52:16', '2023-08-28 01:49:21', '2023-08-28 01:52:16');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (98, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'd919762d6d9df246bfca133fbba38b3c3875bfeca7c32fece786b8f2db59cebc', '["*"]', NULL, '2023-08-24 09:33:49', '2023-08-24 09:33:49');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (109, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '2ff36f9f2045ad2240c1eb24741e112fbf95673c680cc1930815489ece3247fd', '["*"]', '2023-08-29 06:02:48', '2023-08-29 05:36:41', '2023-08-29 06:02:48');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (95, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '71a0913d2b55bdf6888ef1012f8dd232619418c35eccca8d1b5f8c9125cc2960', '["*"]', '2023-08-21 12:38:25', '2023-08-21 12:28:57', '2023-08-21 12:38:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (91, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '0d9d28f51e2c7ff7be527cfcf813b0559f28e350f6acecd22fe767c1e2481e20', '["*"]', '2023-08-21 10:16:03', '2023-08-21 09:51:15', '2023-08-21 10:16:03');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (88, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '75f1ae1945e4c2c577810677a1f2907a9e5ffd92653c30f78f43eab4fd05470f', '["*"]', '2023-08-21 08:06:01', '2023-08-21 07:37:52', '2023-08-21 08:06:01');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (107, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '370a4dc05bda52f29525a7e9b3b90d4bb733b411a16e76d787741bbb2f3b9c9a', '["*"]', '2023-08-28 02:01:32', '2023-08-28 01:55:50', '2023-08-28 02:01:32');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (101, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'aed37c53465ecb543dba8ea7a071f212e2e3db1c914e9803d351e93c5f026f25', '["*"]', NULL, '2023-08-24 09:35:29', '2023-08-24 09:35:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (111, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '6a6301d4fb0b44582b7c66a2492af35f0588fc23f3cfab51af9ca33d89453654', '["*"]', NULL, '2023-08-29 06:07:24', '2023-08-29 06:07:24');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (84, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '0c1526f4a6a5c6993d8cb3fc9400f641c6fb2bf443f170d70b4184521063b2fc', '["*"]', '2023-08-21 06:44:19', '2023-08-21 06:17:22', '2023-08-21 06:44:19');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (99, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '7c9bd452e7e455662c8c364fa859606b4aa072f70ebf3efe4bd4f4c82cf231fe', '["*"]', NULL, '2023-08-24 09:34:29', '2023-08-24 09:34:29');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (96, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'dee01d04006cd03ee52931d5c6e52bc2a50e7f9d719b71e97e82fcd69b714de7', '["*"]', '2023-08-23 22:39:49', '2023-08-23 22:39:34', '2023-08-23 22:39:49');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (92, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'bbc671aaea6379804ddb1563c6f1a1d781671c0b5ada71eb52aa195197018623', '["*"]', '2023-08-21 10:31:51', '2023-08-21 10:28:25', '2023-08-21 10:31:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (93, 'App\Models\User', 10, 'alihamzahar1@gmail.com', '11123acd60d91c5603ecf7b9ce632ba51901aad3a37abd2fcb23eb10725a7bd9', '["*"]', NULL, '2023-08-21 11:34:51', '2023-08-21 11:34:51');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (89, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'd273adcc36bf1e9644cd54e3a184a480a37d725a004d37d0e1f7bea1b19cf316', '["*"]', '2023-08-21 08:11:25', '2023-08-21 08:08:28', '2023-08-21 08:11:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (90, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'd28e57fe58191d980a39c128a16893180eedb19e86a77f69fca8ffe846a171aa', '["*"]', NULL, '2023-08-21 09:51:14', '2023-08-21 09:51:14');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (112, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'b0ea0e3269d1deaacfa2054c79cc24a8cbd6ed38dc414aae9409b73869195217', '["*"]', '2023-08-29 06:25:25', '2023-08-29 06:07:24', '2023-08-29 06:25:25');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (100, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', 'bc3aad847613df189d6159df86c5ba64e81c89085d1ce1fa5c228764763a3bdb', '["*"]', NULL, '2023-08-24 09:34:55', '2023-08-24 09:34:55');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (108, 'App\Models\User', 7, 'ali.dev.jkt@gmail.com', '0c6141c63f481cd26d1467f34ae74de99f9d930f13f57783cd389380ac5c9f7a', '["*"]', NULL, '2023-08-28 01:56:04', '2023-08-28 01:56:04');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (105, 'App\Models\User', 11, 'alihamzah@gmail.com', '3896bfa0f1a326174195a78e18e19c2795be3ed09de61c8f0f3be6a39cefaa8f', '["*"]', NULL, '2023-08-28 01:52:11', '2023-08-28 01:52:11');
INSERT INTO "vnd_mgm"."personal_access_tokens" VALUES (114, 'App\Models\User', 10, 'alihamzahar1@gmail.com', 'e7f2ff51a083a3ab4d46a76fa4558374380dafb8782329b1b3c989bbadc37121', '["*"]', '2023-08-29 07:54:54', '2023-08-29 07:35:34', '2023-08-29 07:54:54');

-- ----------------------------
-- Table structure for trx_afiliasi
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_afiliasi";
CREATE TABLE "vnd_mgm"."trx_afiliasi" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "identity_number" varchar(100) COLLATE "pg_catalog"."default",
  "vendor_name" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_afiliasi
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_afiliasi" VALUES ('024da90b-ffad-4800-842b-ce6e13d5ae0c', 7, '1291920001', 'Vendor ABC', '2023-08-21 06:04:01', '2023-08-21 06:04:01');
INSERT INTO "vnd_mgm"."trx_afiliasi" VALUES ('1366b435-15ba-413d-913b-09d610a9b1cf', 10, '18291002001', 'Dimensi Persada, PT', '2023-08-21 08:04:55', '2023-08-21 08:04:55');

-- ----------------------------
-- Table structure for trx_akta_vnd
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_akta_vnd";
CREATE TABLE "vnd_mgm"."trx_akta_vnd" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "akta_type" varchar(100) COLLATE "pg_catalog"."default",
  "notaris_name" varchar(255) COLLATE "pg_catalog"."default",
  "akta_number" varchar(100) COLLATE "pg_catalog"."default",
  "date" date,
  "ahu_number" varchar(150) COLLATE "pg_catalog"."default",
  "ahu_date" date,
  "document" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "user_id" int4
)
;

-- ----------------------------
-- Records of trx_akta_vnd
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_akta_vnd" VALUES ('0e3a11d7-5468-4af8-9810-d7c99d8fe795', 'Akta Perubahan', 'Ilham J', '91921002', '2023-08-21', '17281991', '2023-08-21', '1692599939_sample.pdf', '2023-08-21 06:38:59', '2023-08-21 06:38:59', 7);
INSERT INTO "vnd_mgm"."trx_akta_vnd" VALUES ('9bbec7e3-2905-455d-a4d1-ddaa92018288', 'Akta Perubahan', 'Deni', '819291991', '2023-08-21', '192912010', '2023-08-21', '1692605395_sample.pdf', '2023-08-21 08:09:55', '2023-08-21 08:09:55', 10);

-- ----------------------------
-- Table structure for trx_doc_pendukung
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_doc_pendukung";
CREATE TABLE "vnd_mgm"."trx_doc_pendukung" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "letters" varchar(255) COLLATE "pg_catalog"."default",
  "number" varchar(255) COLLATE "pg_catalog"."default",
  "periode_date" varchar(255) COLLATE "pg_catalog"."default",
  "document" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_doc_pendukung
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_doc_pendukung" VALUES ('0fbc9228-0f0b-4c8d-ad9e-dd5b3c24d30d', 7, 'Sertifkasi Surat', '19219291001', '2023-08-21', 'uploads/1692597968_sample.pdf', '2023-08-21 06:06:08', '2023-08-21 06:06:08');
INSERT INTO "vnd_mgm"."trx_doc_pendukung" VALUES ('288ca0ab-9f08-41f8-9632-6fa35c422168', 10, 'SERTIFIKAT A', '19010032', '2023-08-21', 'uploads/1692605352_sample.pdf', '2023-08-21 08:09:12', '2023-08-21 08:09:12');

-- ----------------------------
-- Table structure for trx_kepemilikan_vnd
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_kepemilikan_vnd";
CREATE TABLE "vnd_mgm"."trx_kepemilikan_vnd" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "designation" varchar(255) COLLATE "pg_catalog"."default",
  "pic_name" varchar(255) COLLATE "pg_catalog"."default",
  "ownership" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "user_id" int4,
  "pic_position" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of trx_kepemilikan_vnd
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_kepemilikan_vnd" VALUES ('2b29d072-8e2e-4005-81ab-eb85425f485c', '-', 'Ridho Illahi', '80%', '2023-08-08 04:05:49', '2023-08-08 04:05:49', 6, NULL);
INSERT INTO "vnd_mgm"."trx_kepemilikan_vnd" VALUES ('4307943d-048b-4405-8934-34d736dfa5a9', '-', 'Abdullah Illahi', '80%', '2023-08-08 04:05:56', '2023-08-08 04:05:56', 6, NULL);
INSERT INTO "vnd_mgm"."trx_kepemilikan_vnd" VALUES ('2cefc600-3059-49a8-ba37-72449ebb6c7c', 'Mr', 'Diki P', '80', '2023-08-16 04:50:31', '2023-08-16 04:50:31', 7, 'Komisaris');
INSERT INTO "vnd_mgm"."trx_kepemilikan_vnd" VALUES ('b9ab4a7c-2c1e-4da4-8449-cf89717334a4', 'Mr', 'Hari', '75', '2023-08-21 06:01:58', '2023-08-21 06:01:58', 7, 'Engginer');
INSERT INTO "vnd_mgm"."trx_kepemilikan_vnd" VALUES ('fa0f4609-e100-475d-9da1-18ac1409a12d', 'Mr', 'Diki P', '75', '2023-08-21 08:02:12', '2023-08-21 08:02:12', 10, 'Direktur');

-- ----------------------------
-- Table structure for trx_kompetensi_ref_vnd
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_kompetensi_ref_vnd";
CREATE TABLE "vnd_mgm"."trx_kompetensi_ref_vnd" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "competence_code" varchar(255) COLLATE "pg_catalog"."default",
  "competence" varchar(255) COLLATE "pg_catalog"."default",
  "experience" varchar(255) COLLATE "pg_catalog"."default",
  "status" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_kompetensi_ref_vnd
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_kompetensi_ref_vnd" VALUES ('3d0150f5-3d85-43cb-acc0-53a447ecf713', 7, '812910001', 'Kompetensi A', '2 TAHUN', 'Baik', '2023-08-15 13:00:59', '2023-08-15 13:00:59');
INSERT INTO "vnd_mgm"."trx_kompetensi_ref_vnd" VALUES ('50cb7986-9a6c-47b4-b8dc-26e615e19bff', 10, '468291992191', 'IT', '2 Tahun', 'Baik', '2023-08-21 08:05:34', '2023-08-21 08:05:34');

-- ----------------------------
-- Table structure for trx_legal_finance
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_legal_finance";
CREATE TABLE "vnd_mgm"."trx_legal_finance" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "periode_tahun_buku" varchar(100) COLLATE "pg_catalog"."default",
  "jumlah_karyawan" varchar(100) COLLATE "pg_catalog"."default",
  "nomor_bpkm" varchar(255) COLLATE "pg_catalog"."default",
  "periode_bpkm" varchar(100) COLLATE "pg_catalog"."default",
  "masa_berlaku_bpkm" date,
  "document_bpkm" varchar(255) COLLATE "pg_catalog"."default",
  "nomor_nib" varchar(255) COLLATE "pg_catalog"."default",
  "masa_berlaku_nib" varchar(100) COLLATE "pg_catalog"."default",
  "tanggal_nib" date,
  "document_nib" varchar(255) COLLATE "pg_catalog"."default",
  "nomor_domisili" varchar(255) COLLATE "pg_catalog"."default",
  "masa_berlaku_domisili" varchar(100) COLLATE "pg_catalog"."default",
  "tanggal_domisili" date,
  "document_domisili" varchar(255) COLLATE "pg_catalog"."default",
  "nomor_npwp" varchar(255) COLLATE "pg_catalog"."default",
  "alamat_npwp" text COLLATE "pg_catalog"."default",
  "document_npwp" varchar(255) COLLATE "pg_catalog"."default",
  "status_pajak" varchar(255) COLLATE "pg_catalog"."default",
  "document_pajak" varchar(255) COLLATE "pg_catalog"."default",
  "masa_berlaku_pajak" varchar(100) COLLATE "pg_catalog"."default",
  "bukti_lapor_pph" varchar(255) COLLATE "pg_catalog"."default",
  "bukti_lapor_spt" varchar(255) COLLATE "pg_catalog"."default",
  "aset_lancar" varchar(255) COLLATE "pg_catalog"."default",
  "kewajiban_jangka_pendek" varchar(255) COLLATE "pg_catalog"."default",
  "document_keuangan" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_legal_finance
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_legal_finance" VALUES ('21a7b391-7339-4ede-81a7-6c2ed3c6e33c', 7, '2 tahun', '10', '12900021', '2023-08-21', '2023-08-28', '1693188070_sample.pdf', '123456', '2023-08-28', '2023-08-28', '1693188070_sample.pdf', '123456', '2023-08-28', '2023-08-28', '1693188070_sample.pdf', '123456', 'Jakarta', '1693188070_sample.pdf', 'test', '1693188070_sample.pdf', '2023-08-28', '1693188070_sample.pdf', '1693188070_sample.pdf', '-', '-', '1693188070_sample.pdf', '2023-08-28 02:01:10', '2023-08-28 02:01:10');

-- ----------------------------
-- Table structure for trx_master_business_qualifications
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_master_business_qualifications";
CREATE TABLE "vnd_mgm"."trx_master_business_qualifications" (
  "id" int8 NOT NULL DEFAULT nextval('"vnd_mgm".trx_master_business_qualifications_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_master_business_qualifications
-- ----------------------------

-- ----------------------------
-- Table structure for trx_pic_vnd
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_pic_vnd";
CREATE TABLE "vnd_mgm"."trx_pic_vnd" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "designation" varchar(200) COLLATE "pg_catalog"."default",
  "pic_name" varchar(255) COLLATE "pg_catalog"."default",
  "pic_position" varchar(100) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "no_identity" varchar(255) COLLATE "pg_catalog"."default",
  "periode_time" varchar(100) COLLATE "pg_catalog"."default",
  "address" text COLLATE "pg_catalog"."default",
  "npwp" varchar(255) COLLATE "pg_catalog"."default",
  "phone" varchar(100) COLLATE "pg_catalog"."default",
  "user_id" int4
)
;

-- ----------------------------
-- Records of trx_pic_vnd
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_pic_vnd" VALUES ('0c16fff1-04d4-48ee-a901-cd8ce72445d7', NULL, 'Ridho Illahi', 'Manager', 'albasys@albasys.net', '2023-08-08 03:19:50', '2023-08-08 03:19:50', '32089291000921', '2 Tahun', 'Jalan Mampang Prapatan No. 58 Jakarta Selatan', '3128391991', '08192910001', 6);
INSERT INTO "vnd_mgm"."trx_pic_vnd" VALUES ('89b1f5c1-99e2-4688-8d7a-03ac6e37b384', NULL, 'Ahmed R', 'Direktur', 'albasys@albasys.net', '2023-08-08 03:21:29', '2023-08-08 03:21:29', '32089291000921', '1 Tahun', 'Jalan Rasuna Said Jakarta Selatan', '3128391991', '08192910001', 6);
INSERT INTO "vnd_mgm"."trx_pic_vnd" VALUES ('cdff16e4-7d93-4395-a1c8-2d9b44a35cff', NULL, 'Diki Permadi', 'Komisaris', 'dikip@gmail.com', '2023-08-16 04:49:41', '2023-08-16 04:49:41', '81291001201', '2 Tahun', 'Bogor Raya', '18291020012', '1920102001', 7);
INSERT INTO "vnd_mgm"."trx_pic_vnd" VALUES ('9c08d87b-c3e3-426b-8789-9cc2ea9ea97f', NULL, 'ALi Hamzah', 'Teknisi', 'ali.dev.jkt@gmail.com', '2023-08-21 06:00:17', '2023-08-21 06:00:17', '892102001', '2 Tahun', 'Jalan mampang prapatan XI', '18291020001', '1829102001', 7);
INSERT INTO "vnd_mgm"."trx_pic_vnd" VALUES ('58e8b29a-7fa4-4cd2-8c27-c451b2e43cd5', NULL, 'Diki P', 'Direktur', 'dikip@gmail.com', '2023-08-21 08:01:26', '2023-08-21 08:01:26', '1829100102', '2 Tahun', 'Jakarta, Mampang prapatan No. XI', '128192001', '0892910201', 10);

-- ----------------------------
-- Table structure for trx_principal
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_principal";
CREATE TABLE "vnd_mgm"."trx_principal" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "vendor_name" varchar(255) COLLATE "pg_catalog"."default",
  "product_name" varchar(255) COLLATE "pg_catalog"."default",
  "periode_date" varchar(255) COLLATE "pg_catalog"."default",
  "document" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of trx_principal
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_principal" VALUES ('b9b5643b-2643-4fbb-9f46-835557910bf1', 7, 'Vendor A', '9012000120', '2023-08-15', NULL, '2023-08-15 13:01:37', '2023-08-15 13:01:37');
INSERT INTO "vnd_mgm"."trx_principal" VALUES ('124f7b6e-d330-4f1b-8c88-db603506bc18', 10, 'Dimensi Persada', 'Software', '2023-08-21', 'uploads/1692605160_sample.pdf', '2023-08-21 08:06:00', '2023-08-21 08:06:00');

-- ----------------------------
-- Table structure for trx_sign_contract_vnd
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_sign_contract_vnd";
CREATE TABLE "vnd_mgm"."trx_sign_contract_vnd" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "pic_position" varchar(255) COLLATE "pg_catalog"."default",
  "pic_name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "user_id" int4
)
;

-- ----------------------------
-- Records of trx_sign_contract_vnd
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_sign_contract_vnd" VALUES ('a4f8699d-ee96-4f26-b9bd-d19420b2c09a', 'Komisaris', 'Diki P', 'dikip@gmail.com', '2023-08-16 04:50:08', '2023-08-16 04:50:08', 7);
INSERT INTO "vnd_mgm"."trx_sign_contract_vnd" VALUES ('54098aeb-1969-4f60-935e-15f4d0a963cb', 'Engginer', 'Ali Hamzah', 'ali.dev.jkt@gmail.com', '2023-08-21 06:01:32', '2023-08-21 06:01:32', 7);
INSERT INTO "vnd_mgm"."trx_sign_contract_vnd" VALUES ('d265a3c5-8577-4024-a3e8-1d29571ec27d', 'Direktur', 'Diki P', 'dikip@gmail.com', '2023-08-21 08:01:52', '2023-08-21 08:01:52', 10);

-- ----------------------------
-- Table structure for trx_vnd_bank
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_vnd_bank";
CREATE TABLE "vnd_mgm"."trx_vnd_bank" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "rekening_holder" varchar(255) COLLATE "pg_catalog"."default",
  "rekening_number" varchar(100) COLLATE "pg_catalog"."default",
  "currency" varchar(100) COLLATE "pg_catalog"."default",
  "bank_name" varchar(100) COLLATE "pg_catalog"."default",
  "bank_chapter" varchar(100) COLLATE "pg_catalog"."default",
  "clearing_code" varchar(100) COLLATE "pg_catalog"."default",
  "city" varchar(255) COLLATE "pg_catalog"."default",
  "primary" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "user_id" int4
)
;

-- ----------------------------
-- Records of trx_vnd_bank
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_vnd_bank" VALUES ('9d08e279-9da8-48a4-8937-00cd122468c7', 'Ali Hamzah', '9192010210', 'IDR', 'BRI', 'JAKARTA', '1920101', 'JAKARTA', NULL, '2023-08-10 14:59:44', '2023-08-10 14:59:44', 9);
INSERT INTO "vnd_mgm"."trx_vnd_bank" VALUES ('737b3c60-fbb1-4c59-972c-58588abc965f', 'Ali Hamzah AR', '9192010210', 'IDR', 'BRI', 'JAKARTA', '1920101', 'JAKARTA', NULL, '2023-08-10 15:00:00', '2023-08-10 15:00:00', 9);
INSERT INTO "vnd_mgm"."trx_vnd_bank" VALUES ('e2300ab6-ca90-4a20-8aa7-ecb9efb8e88e', 'Ibrahim S', '19210001', 'IDR', 'BRI', 'JAKARTA', '912910', 'JAKARTA', NULL, '2023-08-10 15:00:27', '2023-08-10 15:00:27', 9);
INSERT INTO "vnd_mgm"."trx_vnd_bank" VALUES ('e6e94d46-258f-4b28-8a17-6a6ba5d4daa4', 'Deni', '182192010021', 'IDR', 'BCA', 'Mampang Prapatan', '121929191', 'Jakarta', '1', '2023-08-16 04:53:13', '2023-08-16 04:53:13', 7);
INSERT INTO "vnd_mgm"."trx_vnd_bank" VALUES ('b2a12772-bbb4-4ed4-8385-5a8d06aab73b', 'Diki p', '8910201001', 'IDR', 'BNI', 'Jakarta', '18291991', 'DKI Jakarta', '1', '2023-08-21 08:04:35', '2023-08-21 08:04:35', 10);

-- ----------------------------
-- Table structure for trx_vnd_general_info
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_vnd_general_info";
CREATE TABLE "vnd_mgm"."trx_vnd_general_info" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" int8 NOT NULL,
  "regis_location" varchar(255) COLLATE "pg_catalog"."default",
  "vendor_name" varchar(255) COLLATE "pg_catalog"."default",
  "type_bussines" varchar(100) COLLATE "pg_catalog"."default",
  "npwp" varchar(100) COLLATE "pg_catalog"."default",
  "npwp_address" varchar(255) COLLATE "pg_catalog"."default",
  "street" text COLLATE "pg_catalog"."default",
  "country" varchar(100) COLLATE "pg_catalog"."default",
  "province" varchar(200) COLLATE "pg_catalog"."default",
  "city" varchar(150) COLLATE "pg_catalog"."default",
  "postal_code" varchar(100) COLLATE "pg_catalog"."default",
  "phone_office" varchar(100) COLLATE "pg_catalog"."default",
  "code_area" varchar(100) COLLATE "pg_catalog"."default",
  "fax_number" varchar(100) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "rt" varchar(255) COLLATE "pg_catalog"."default",
  "rw" varchar(255) COLLATE "pg_catalog"."default",
  "village" varchar(255) COLLATE "pg_catalog"."default",
  "business_qualifications" varchar(255) COLLATE "pg_catalog"."default",
  "district" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of trx_vnd_general_info
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_vnd_general_info" VALUES ('132a52f5-74b8-4f92-a547-b7724bfd0848', 11, 'indonesia', 'Dimensi Persada, PT', 'IT', '123456', 'Jalan Dr. Satrio Mega Kuningan Jakarta Selatans', 'Mega Kuningan Jakarta', 'Indonesia', 'DKI Jakarta', 'Jakarta Selatan', '16217', '0829190012', '-', '-', 'alihamzah@gmail.com', '2023-08-28 01:51:40', '2023-08-28 01:51:40', '001', '002', 'Kuningan', 'menengah', 'Kuningan');

-- ----------------------------
-- Table structure for trx_vnd_pic_contact
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."trx_vnd_pic_contact";
CREATE TABLE "vnd_mgm"."trx_vnd_pic_contact" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "pic_name" varchar(255) COLLATE "pg_catalog"."default",
  "pic_position" varchar(100) COLLATE "pg_catalog"."default",
  "type" varchar(100) COLLATE "pg_catalog"."default",
  "phone" varchar(100) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "user_id" int4
)
;

-- ----------------------------
-- Records of trx_vnd_pic_contact
-- ----------------------------
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('deb0c62b-557c-4e44-8377-ae5143fd2aa1', 'OK', 'OK', NULL, NULL, 'OK', '2023-08-11 01:20:15', '2023-08-11 01:20:15', 7);
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('fd76f541-341f-4b01-b8c9-e7a1db84eb15', 'Ali Hamzah', 'Teknisi', NULL, NULL, 'alihamzahar@gmail.com', '2023-08-11 07:27:20', '2023-08-11 07:27:20', 7);
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('fcfbf94a-97d7-4a92-a35d-aa2f08f46e6d', 'ALi Hamzah', 'Teknisi', 'account_manager', NULL, 'alihamzahar@gmail.com', '2023-08-11 08:55:52', '2023-08-11 08:55:52', 7);
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('3ff663e9-afb8-4f54-b0ee-eb30e56006c4', 'Dani Hidayat', 'Teknisi', 'finance', '0891920012', 'dani@gmail.com', '2023-08-15 12:31:53', '2023-08-15 12:31:53', 7);
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('ff84210e-c84b-4134-aa64-620fe1e4f204', 'Dani', 'Account Manager', 'account_manager', '087182909192', 'dani@gmail.com', '2023-08-21 08:03:39', '2023-08-21 08:03:39', 10);
INSERT INTO "vnd_mgm"."trx_vnd_pic_contact" VALUES ('3b3b6550-3279-4d60-b6a4-8e3d06460828', 'Hari', 'Finance', 'finance', '08192910021', 'hari@gmail.com', '2023-08-21 08:04:05', '2023-08-21 08:04:05', 10);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "vnd_mgm"."users";
CREATE TABLE "vnd_mgm"."users" (
  "id" int8 NOT NULL DEFAULT nextval('"vnd_mgm".users_id_seq'::regclass),
  "vendor_type_id" uuid,
  "sub_type_vendor_id" uuid,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamp(0),
  "npwp" varchar(255) COLLATE "pg_catalog"."default",
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "profesi" varchar(255) COLLATE "pg_catalog"."default",
  "kewarganegaraan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "vnd_mgm"."users" VALUES (7, '9580e7bc-6841-467b-8257-7aec651ae7f9', '9580e7bc-6841-467b-8257-7aec651ae7f9', NULL, 'ali.dev.jkt@gmail.com', NULL, '98102819021', '$2y$10$n10LgxV19cqmgww0mZ8AHe44oa/q2veU6xFv7C16K9fDnABDMjTMa', NULL, '2023-08-09 13:50:06', '2023-08-09 13:50:06', NULL, NULL);
INSERT INTO "vnd_mgm"."users" VALUES (8, '9580e7bc-6841-467b-8257-7aec651ae7f9', '955f342e-540d-454b-80c8-e8da9e69440a', NULL, 'alihamzahar@gmail.com', NULL, '12100210201', '$2y$10$cgT7QTr2pUXm8tRe09B3eeqVfk96aLRgZbYGtXoNTvT2D.2wNAxl2', NULL, '2023-08-10 03:36:45', '2023-08-10 03:36:45', NULL, NULL);
INSERT INTO "vnd_mgm"."users" VALUES (9, '9580e7bc-6841-467b-8257-7aec651ae7f9', '955f342e-540d-454b-80c8-e8da9e69440a', NULL, 'test@gmail.com', NULL, '12345678', '$2y$10$5omax30OFdk68Pw.KmPB0.cI/cVGQrG5BD2m9s8.Kh83QlJzY2OKC', NULL, '2023-08-10 09:25:03', '2023-08-10 09:25:03', NULL, NULL);
INSERT INTO "vnd_mgm"."users" VALUES (10, '9580e7bc-6841-467b-8257-7aec651ae7f9', '955f342e-540d-454b-80c8-e8da9e69440a', NULL, 'alihamzahar1@gmail.com', NULL, '18219912001', '$2y$10$v3EOOpf9k4lr5ADHE/f2cOBpOs.7j09chnDaaNeHqp/BGqixPutwe', NULL, '2023-08-21 07:33:41', '2023-08-21 07:33:41', NULL, NULL);
INSERT INTO "vnd_mgm"."users" VALUES (11, '9580e7bc-6841-467b-8257-7aec651ae7f9', '955f342e-540d-454b-80c8-e8da9e69440a', NULL, 'alihamzah@gmail.com', NULL, '123456', '$2y$10$m1TcJOs7fZ154PEOZ/cBSOtBSt7xU3urrl3RYwsFK92GFoucL8X0u', NULL, '2023-08-28 01:49:09', '2023-08-28 01:49:09', NULL, NULL);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."failed_jobs_id_seq"
OWNED BY "vnd_mgm"."failed_jobs"."id";
SELECT setval('"vnd_mgm"."failed_jobs_id_seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."migrations_id_seq"
OWNED BY "vnd_mgm"."migrations"."id";
SELECT setval('"vnd_mgm"."migrations_id_seq"', 28, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."mst_pengadaan_vms_id_seq"
OWNED BY "vnd_mgm"."mst_pengadaan_vms"."id";
SELECT setval('"vnd_mgm"."mst_pengadaan_vms_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."personal_access_tokens_id_seq"
OWNED BY "vnd_mgm"."personal_access_tokens"."id";
SELECT setval('"vnd_mgm"."personal_access_tokens_id_seq"', 114, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."trx_master_business_qualifications_id_seq"
OWNED BY "vnd_mgm"."trx_master_business_qualifications"."id";
SELECT setval('"vnd_mgm"."trx_master_business_qualifications_id_seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "vnd_mgm"."users_id_seq"
OWNED BY "vnd_mgm"."users"."id";
SELECT setval('"vnd_mgm"."users_id_seq"', 11, true);

-- ----------------------------
-- Uniques structure for table failed_jobs
-- ----------------------------
ALTER TABLE "vnd_mgm"."failed_jobs" ADD CONSTRAINT "vnd_mgm_failed_jobs_uuid_unique" UNIQUE ("uuid");

-- ----------------------------
-- Primary Key structure for table failed_jobs
-- ----------------------------
ALTER TABLE "vnd_mgm"."failed_jobs" ADD CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "vnd_mgm"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_pengadaan_vms
-- ----------------------------
ALTER TABLE "vnd_mgm"."mst_pengadaan_vms" ADD CONSTRAINT "mst_pengadaan_vms_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mst_vnd_type
-- ----------------------------
ALTER TABLE "vnd_mgm"."mst_vnd_type" ADD CONSTRAINT "trx_vendor_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "vnd_mgm_password_resets_email_index" ON "vnd_mgm"."password_resets" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Indexes structure for table personal_access_tokens
-- ----------------------------
CREATE INDEX "vnd_mgm_personal_access_tokens_tokenable_type_tokenable_id_inde" ON "vnd_mgm"."personal_access_tokens" USING btree (
  "tokenable_type" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "tokenable_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table personal_access_tokens
-- ----------------------------
ALTER TABLE "vnd_mgm"."personal_access_tokens" ADD CONSTRAINT "vnd_mgm_personal_access_tokens_token_unique" UNIQUE ("token");

-- ----------------------------
-- Primary Key structure for table personal_access_tokens
-- ----------------------------
ALTER TABLE "vnd_mgm"."personal_access_tokens" ADD CONSTRAINT "personal_access_tokens_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_afiliasi
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_afiliasi" ADD CONSTRAINT "trx_afiliasi_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_akta_vnd
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_akta_vnd" ADD CONSTRAINT "trx_akta_vnd_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_doc_pendukung
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_doc_pendukung" ADD CONSTRAINT "trx_doc_pendukung_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_kepemilikan_vnd
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_kepemilikan_vnd" ADD CONSTRAINT "trx_kepemilikan_vnd_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_kompetensi_ref_vnd
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_kompetensi_ref_vnd" ADD CONSTRAINT "trx_kompetensi_ref_vnd_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_legal_finance
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_legal_finance" ADD CONSTRAINT "trx_legal_finance_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_master_business_qualifications
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_master_business_qualifications" ADD CONSTRAINT "trx_master_business_qualifications_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_pic_vnd
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_pic_vnd" ADD CONSTRAINT "trx_pic_vnd_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_principal
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_principal" ADD CONSTRAINT "trx_principal_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_sign_contract_vnd
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_sign_contract_vnd" ADD CONSTRAINT "trx_sign_contract_vnd_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_vnd_bank
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_vnd_bank" ADD CONSTRAINT "trx_vnd_bank_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_vnd_general_info
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_vnd_general_info" ADD CONSTRAINT "trx_vnd_general_info_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trx_vnd_pic_contact
-- ----------------------------
ALTER TABLE "vnd_mgm"."trx_vnd_pic_contact" ADD CONSTRAINT "trx_vnd_pic_contact_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "vnd_mgm"."users" ADD CONSTRAINT "vnd_mgm_users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "vnd_mgm"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
