<?php

use App\Http\Controllers\Api\AcountManagerController;
use App\Http\Controllers\Api\AfiliasiController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\BankController;
use App\Http\Controllers\Api\BusinnerQualificationsController;
use App\Http\Controllers\Api\DependantDropdownController;
use App\Http\Controllers\Api\DocPendukungController;
use App\Http\Controllers\Api\FinanceLegalitasController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\VendorTypeController;
use App\Http\Controllers\Api\VendorSubTypeController;
use App\Http\Controllers\Api\GeneralInfoController;
use App\Http\Controllers\Api\KompetensiController;
use App\Http\Controllers\Api\OpportunitiesController;
use App\Http\Controllers\Api\PengadaanController;
use App\Http\Controllers\Api\PicAktaController;
use App\Http\Controllers\Api\PicContactController;
use App\Http\Controllers\Api\PicKepemilikanController;
use App\Http\Controllers\Api\PicVendorController;
use App\Http\Controllers\Api\PrincipalController;
use App\Http\Controllers\Api\ProgressController;
use App\Http\Controllers\Api\SignContractController;
use App\Http\Controllers\Api\StatusVendorController;
use App\Http\Controllers\Api\SummaryController;
use App\Http\Controllers\Api\UserController;
use App\Models\Opportunities;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', \App\Http\Controllers\Api\Auth\RegisterController::class);
Route::post('processlogin', \App\Http\Controllers\Api\Auth\LoginController::class);
Route::apiResource('/vendor/type', VendorTypeController::class);
Route::apiResource('/vendor/subtype', VendorSubTypeController::class);
Route::apiResource('/business_qualifications', BusinnerQualificationsController::class);
Route::get('/user', [UserController::class, 'showUser']);
Route::get('/all/user', [UserController::class, 'showAllUser']);
Route::apiResource('/vendor/berita', PengadaanController::class);
 //Local Indonesia
 Route::get('provinces', [DependantDropdownController::class, 'provinces'])->name('provinces');
 Route::get('cities', [DependantDropdownController::class, 'cities'])->name('cities');
 Route::get('districts', [DependantDropdownController::class, 'districts'])->name('districts');
 Route::get('villages', [DependantDropdownController::class, 'villages'])->name('villages');
Route::middleware(['auth:sanctum'])->group(function () {
    // MASTER
    //Aprove
    Route::put('approve/vendor', [UserController::class, 'updateUserStatus']);
    Route::put('approve/generalinfo', [UserController::class, 'updateGeneralInfoStatus']);
    Route::put('approve/pic', [UserController::class, 'updatePICStatus']);
    Route::put('approve/contact', [UserController::class, 'updateContactStatus']);
    Route::put('approve/bank', [UserController::class, 'updateBankStatus']);
    Route::put('approve/afiliasi', [UserController::class, 'updateAfiliasiStatus']);
    Route::put('approve/trx_kompetensi_ref_vnd', [UserController::class, 'updateKomRefStatus']);
    Route::put('approve/finance-legalitas', [UserController::class, 'updateFinanceLegalitasStatus']);
    //aprove pertab
    Route::put('approve/vendor/tab', [StatusVendorController::class, 'updateStatus']);
    Route::get('approve/vendor/tab', [StatusVendorController::class, 'showUserApprove']);

    Route::put('update/about', [UserController::class, 'updateUserStatus']);
    Route::put('vendor/user', [UserController::class, 'updateUser']);
    Route::apiResource('/vendor/generalinfo', GeneralInfoController::class);
    Route::apiResource('/vendor/list', GeneralInfoController::class);
    Route::get('vendor/generalinfo', [GeneralInfoController::class, 'showUser']);
    Route::apiResource('/vendor/pic', PicVendorController::class);
    Route::get('vendor/pic', [PicVendorController::class, 'showUser']);
    Route::apiResource('/pic/kepemilikan', PicKepemilikanController::class);
    Route::get('pic/kepemilikan', [PicKepemilikanController::class, 'showUser']);
    Route::apiResource('/pic/sign', SignContractController::class);
    Route::get('pic/sign', [SignContractController::class, 'showUser']);
    Route::apiResource('/pic/akta', PicAktaController::class);
    Route::get('pic/akta', [PicAktaController::class, 'showUser']);
    Route::apiResource('/vendor/bank', BankController::class);
    Route::get('vendor/bank', [BankController::class, 'showUser']);
    Route::apiResource('/vendor/account-manager', PicContactController::class);
    Route::get('vendor/account-manager', [PicContactController::class, 'showUser']);
    Route::apiResource('/vendor/account-finance', PicContactController::class);
    Route::get('vendor/account-finance', [PicContactController::class, 'showUser']);
    Route::post('/logout', [LoginController::class, 'logout']);
    Route::apiResource('/vendor/afiliasi', AfiliasiController::class);
    Route::get('vendor/afiliasi', [AfiliasiController::class, 'showUser']);
    Route::apiResource('/vendor/ref_kompetensi', KompetensiController::class);
    Route::get('vendor/ref_kompetensi', [KompetensiController::class, 'showUser']);
    Route::apiResource('/vendor/principal', PrincipalController::class);
    Route::get('vendor/principal', [PrincipalController::class, 'showUser']);
    Route::apiResource('/vendor/doc_pendukung', DocPendukungController::class);
    Route::get('vendor/doc_pendukung', [DocPendukungController::class, 'showUser']);
    Route::apiResource('/vendor/legal_finance', FinanceLegalitasController::class);
    Route::get('vendor/legal_finance', [FinanceLegalitasController::class, 'showUser']);
    Route::apiResource('/vendor/summary', SummaryController::class);
    Route::get('vendor/summary', [SummaryController::class, 'showSummary']);
    Route::get('bobot/kelengkapan', [ProgressController::class, 'showSummary']);
    //Opportunities
    Route::apiResource('/tender/opportunities', OpportunitiesController::class);


});
