<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kompetensi extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_kompetensi_ref_vnd';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'competence_code',
        'competence',
        'experience',
        'status',
        'status_approval'
    ];
}
