<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralInfo extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $table = 'vnd_mgm.trx_vnd_general_info';
    protected $fillable = [
        "user_id",
        "regis_location",
        "business_qualifications",
        "vendor_name",
        "type_bussines",
        "npwp",
        "npwp_address",
        "street",
        "country",
        "province",
        "city",
        "district",
        "village",
        "rt",
        "rw",
        "postal_code",
        "phone_office",
        "code_area",
        "fax_number",
        "email",
        "status"
    ];

}
