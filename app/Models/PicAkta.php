<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicAkta extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_akta_vnd';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id', 'akta_type', 'notaris_name', 'akta_number', 'date', 'ahu_number', 'ahu_date', 'document'
    ];
}
