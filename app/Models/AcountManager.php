<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcountManager extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_acount_manager';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'name',
        'position',
        'identity',
        'identity_number',
        'email',
    ];
}
