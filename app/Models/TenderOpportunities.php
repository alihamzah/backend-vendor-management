<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenderOpportunities extends Model
{
    use HasFactory;
    protected $primaryKey = 'uuid';

    // tell Eloquent that uuid is a string, not an integer
    protected $keyType = 'string';
    protected $table = 'vnd_mgm.tender_opportunities';
}
