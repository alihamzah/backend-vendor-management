<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengadaan extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.mst_pengadaan_vms';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        "pengadaan_name",
        "pengadaan_date",
    ];
}
