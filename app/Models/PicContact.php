<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicContact extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_vnd_pic_contact';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['user_id', 'pic_position', 'pic_name','type','phone', 'email', 'status'];
}

