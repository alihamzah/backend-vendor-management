<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Afiliasi extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_afiliasi';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'identity_number',
        'vendor_name',
        'status'
    ];
}
