<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicKepemilikan extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_kepemilikan_vnd';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['user_id', 'designation', 'pic_name', 'ownership','pic_position','status'];
}
