<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocPendukung extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_doc_pendukung';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'letters',
        'number',
        'periode_date',
        'document',
    ];
}
