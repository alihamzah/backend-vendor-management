<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinanceLegalitas extends Model
{
    use HasFactory;

    protected $table = 'vnd_mgm.trx_legal_finance';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'periode_tahun_buku',
        'jumlah_karyawan',
        'nomor_bpkm',
        'periode_bpkm',
        'masa_berlaku_bpkm',
        'document_bpkm',
        'nomor_nib',
        'masa_berlaku_nib',
        'tanggal_nib',
        'document_nib',
        'nomor_domisili',
        'masa_berlaku_domisili',
        'tanggal_domisili',
        'document_domisili',
        'nomor_npwp',
        'alamat_npwp',
        'document_npwp',
        'status_pajak',
        'document_pajak',
        'masa_berlaku_pajak',
        'bukti_lapor_pph',
        'bukti_lapor_spt',
        'aset_lancar',
        'kewajiban_jangka_pendek',
        'document_keuangan',
        'status'
    ];


}
