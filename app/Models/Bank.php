<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $table = 'vnd_mgm.trx_vnd_bank';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'rekening_holder',
        'rekening_number',
        'currency',
        'bank_name',
        'bank_chapter',
        'clearing_code',
        'city',
        'primary',
        'status'
    ];
}
