<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusVendor extends Model
{

    use HasFactory;
    protected $table = 'vnd_mgm.mst_vnd_status';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['user_id','status_general_info','status_penanggung_jawab','status_pic','status_bank','status_affiliasi','status_kompetensi','status_legal_finance','tanggal_approve'];
}
