<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opportunities extends Model
{

    use HasFactory;
    protected $table = 'vnd_mgm.mst_tender_opportunities';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'vendor_name', 'vendor_category', 'vendor_cualification', 'date_announce','date_close'
    ];
}
