<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'vnd_mgm.users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'vendor_type_id',
        'sub_type_vendor_id',
        'npwp',
        'kewarganegaraan',
        'profesi',
        'aboutus',
        'status'
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];
    public function vendorType()
    {
        return $this->belongsTo(VendorType::class, 'vendor_type_id', 'id');
    }

}

