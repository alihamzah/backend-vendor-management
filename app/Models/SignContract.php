<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SignContract extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_sign_contract_vnd';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['user_id', 'pic_position', 'pic_name', 'email','document','status'];
}
