<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorType extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'vendor_type',
    ];

    protected $table = 'vnd_mgm.mst_vnd_type';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function users()
    {
        return $this->hasMany(User::class, 'vendor_type_id');
    }
}

