<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PicVendor extends Model
{
    use HasFactory;
    protected $table = 'vnd_mgm.trx_pic_vnd';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'user_id',
        'pic_name',
        'pic_position',
        'no_identity',
        'periode_time',
        'address',
        'npwp',
        'email',
        'phone',
        'status',
    ];


}
