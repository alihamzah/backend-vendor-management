<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataControllerApi extends Controller
{
    public function getVendorType()
    { return response()->json('he');

        // $data = DB::table('vnd_mgm.mst_vnd_type')->get();
        // return response()->json($data);
        // if (Auth::guard('sanctum')->check()) {
        //     // Token akses valid, lakukan sesuatu di sini
        //     // ...
        //     return response()->json(['message' => 'Token akses valid']);
        // } else {
        //     // Token akses tidak valid
        //     return response()->json(['message' => 'Token akses tidak valid'], 401);
        // }
    }

    public function getSubVendorType(){
        $data = DB::table('vnd_mgm.mst_sub_vnd_type')->get();
        return response()->json($data);
    }
}
