<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SignContractResource;
use App\Models\SignContract;
use Illuminate\Http\Request;
use File;
class SignContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'user_id' => ['required'],
            'pic_position' => ['required'],
            'pic_name' => ['required'],
            'email' => ['required', 'email'],

        ]);
        try {
            $filename = null; // Deklarasikan $filename di luar blok if
            if ($request->hasFile('document')) {

                $file = $request->file('document');
                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);
            }

            $data = SignContract::create([
                'user_id' => $request->user_id,
                'pic_position' => $request->pic_position ?? '-',
                'pic_name' => $request->pic_name,
                'email' => $request->email,
                  'document' => $filename ?? null // Menyimpan nama file jika diunggah, atau null jika tidak ada file
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');


		try {
           $data = SignContract::where('user_id', $user_id)->paginate($request->limit)
        ->appends(['limit' => $request->limit]);

        return SignContractResource::collection($data)->additional([
            'status' => 200,
            'information' => 'Success fetching data'
        ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SignContract $sign)
    {
        //
        try{
            if ($request->hasFile('document')) {
                File::delete(public_path('uploads/' . $sign->document));

                $filename = null; // Deklarasikan $filename di luar blok if


                $file = $request->file('document');
                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);

                $sign->update([
                'user_id' => $request->user_id,
                'pic_position' => $request->pic_position ?? '-',
                'pic_name' => $request->pic_name,
                'email' => $request->email,
                'document' => $filename ?? null // Menyimpan nama file jika diunggah, atau null jika tidak ada file
                ]);
            } else {

        $sign->update([
                'user_id' => $request->user_id,
                'pic_position' => $request->pic_position ?? '-',
                'pic_name' => $request->pic_name,
                'email' => $request->email
        ]);
    }
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to update data'
        ], 201);
    } catch (QueryException $exception) {
        return response()->json([
            'success' => false,
            "status" => 500,
            'information' => 'Failed to update data'
        ], 500);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SignContract $sign)
    {
        File::delete(public_path('uploads/' . $sign->document));
        $sign->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
