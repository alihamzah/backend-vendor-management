<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showSummary(Request $request)
	{

        try {
            $arr = [


                     "spph"=> 12,
                     "sph_terkirim"=> 30,
                     "surat_undangan_negosiasi"=> 20,
                     "undangan_aanwidzjing"=> 15,
                     "dokumen_tender_dikirim"=> 20,
                     "pemberitahuan_tender"=> 20,
                     "penetapan_pemenang_tender"=> 10,
                     "document_expired"=> 10,
                     "perjanjian_inprogress"=> 10,
                     "perjanjian_selesai"=> 20,
                     "evaluasi"=> "Baik"


             ];
             return response()->json(['data' => $arr, 'status' => 200, 'information' => 'Success fetching data']);
         } catch (\Exception $th) {
             return response()->json(['status' => 401, 'information' => 'Failed fetching data']);
         }

	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
