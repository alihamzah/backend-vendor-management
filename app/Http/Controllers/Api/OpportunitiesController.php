<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OpportunitieResource;
use App\Models\Opportunities;
use Illuminate\Http\Request;

class OpportunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = Opportunities::paginate($request->limit)
         ->appends(['limit' => $request->limit]);

         return OpportunitieResource::collection($data)->additional([
             'status' => 200,
             'information' => 'Success fetching data'
         ]);
         } catch (\Exception $e) {
             return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'vendor_name' => ['required'],
            'vendor_category' => ['required'],
            'vendor_cualification' => ['required'],
            'date_announce' => ['required'],
            'date_close' => ['required']
        ]);
        try {
            $data = Opportunities::create([
                'vendor_name' => $request->vendor_name,
                'vendor_category' => $request->vendor_category,
                'vendor_cualification' => $request->vendor_cualification,
                'date_announce' => $request->date_announce,
                'date_close' => $request->date_close,
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opportunities $opportunity)
    {
        try{
            $opportunity->update([
                'vendor_name' => $request->vendor_name,
                'vendor_category' => $request->vendor_category,
                'vendor_cualification' => $request->vendor_cualification,
                'date_announce' => $request->date_announce,
                'date_close' => $request->date_close,
            ]);
            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Success to update data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to update data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opportunities $opportunity)
    {
        $opportunity->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
