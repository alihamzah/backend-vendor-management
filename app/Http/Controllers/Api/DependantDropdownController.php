<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DependantDropdownController extends Controller
{
    public function provinces()
    {
        $data = DB::table('indonesia_provinces')->get();

        // Ubah format data menjadi yang diinginkan
        $transformedData = $data->map(function ($item) {
            return [
                'id' => $item->id,
                'code' => $item->code,
                'name' => $item->name,

                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
            ];
        });

        // Konversi koleksi ke array
        $result = $transformedData->toArray();
        return  $result;
    }

    public function cities(Request $request)
    {
        // $cities = \Indonesia::findProvince($request->id, ['cities']);
        $data = DB::table('indonesia_cities')
            ->where('province_code', '=', $request->id)
            ->get();



        $data = $data->map(function ($city) {
            return [
                'id' => $city->id,
                'code'=> $city->code,
                'city' => $city->name,
            ];
        })->toArray();

        return response()->json([
            'data' => $data,
            'status' => 200,
            'information' => 'Success fetching data',
        ]);
    }
    public function districts(Request $request)
    {

        // $districs = \Indonesia::findCity($request->id, ['districts']);
        $districs = DB::table('indonesia_districts')
            ->where('city_code', '=', $request->id)
            ->get();
        $data = $districs->map(function ($ds) {
            return [
                'id' => $ds->id,
                'code'=> preg_replace('/\s+/', '', $ds->code),
                'district' => $ds->name,
            ];
        })->toArray();

        return response()->json([
            'data' => $data,
            'status' => 200,
            'information' => 'Success fetching data',
        ]);
    }

    public function villages(Request $request)
    {
        //   $villages = \Indonesia::findDistrict($request->id, ['villages']);
        $villages = DB::table('indonesia_villages')
            ->where('district_code', '=', $request->id)
            ->get();
        $data = $villages->map(function ($vg) {
            return [
                'id' => $vg->id,
                'code'=> $vg->code,
                'valage' => $vg->name,
            ];
        })->toArray();

        return response()->json([
            'data' => $data,
            'status' => 200,
            'information' => 'Success fetching data',
        ]);
    }
}
