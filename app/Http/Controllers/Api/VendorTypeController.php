<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\VendorTypeResource;
use Illuminate\Support\Facades\DB;
use App\Models\VendorType;

class VendorTypeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {

        try {
            $data = VendorType::all();
            $arr = [];
            foreach ($data as $key => $val) {
                $arrx["id"] = $val->id;
                $arrx["vendor_type"] = $val->vendor_type;
                $arr[] = $arrx;
            }
            return response()->json(['data' => $arr, 'status' => 200, 'information' => 'Success fetching data']);
        } catch (\Exception $th) {
            return response()->json(['status' => 401, 'information' => 'Failed fetching data']);
        }

    }
}
