<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Resources\GeneralInfoResource;
use App\Models\GeneralInfo;
use Illuminate\Support\Facades\DB; // Import the DB class at the top of your file

class GeneralInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->input('id');
        if(empty($id)){
            $data = GeneralInfo::paginate($request->limit)
            ->appends(['limit' => $request->limit]);
            return GeneralInfoResource::collection($data)->additional([
                "status"=> 200,
                "information"=> "Success fetching data",
            ]);
        }else{
            $data = GeneralInfo::findOrFail($id);

            return response()->json([
                'data' => $data,
                'status' => 200,
                'information' => 'Success fetching data'
            ], 200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'user_id' => ['required', 'unique:trx_vnd_general_info'],
            'regis_location' => ['required'],
            'business_qualifications' => ['required'],
            'vendor_name' => ['required'],
            'type_bussines' => ['required'],
            'npwp' => ['required'],
            'npwp_address' => ['required'],
            'street' => ['required'],
            'country' => ['required'],
            'province' => ['required'],
            'city' => ['required'],
            'district' => ['required'],
            'village' => ['required'],
            'rt' => ['required'],
            'rw' => ['required'],
            'postal_code' => ['required'],
            'phone_office' => ['required'],
            'code_area' => ['required'],
            'fax_number' => ['required'],
            'email' => ['required', 'email'],
        ]);

        try {
            $data = GeneralInfo::create([
                "user_id" => $request->user_id,
                "business_qualifications" => $request->business_qualifications,
                "regis_location" => $request->regis_location,
                "vendor_name" => $request->vendor_name,
                "type_bussines" => $request->type_bussines,
                "npwp" => $request->npwp,
                "npwp_address" => $request->npwp_address,
                "street" => $request->street,
                "country" => $request->country,
                "province" => $request->province,
                "city" => $request->city,
                "district" => $request->district,
                "village" => $request->village,
                "postal_code" => $request->postal_code,
                "rw" => $request->rw,
                "rt" => $request->rt,
                "phone_office" => $request->phone_office,
                "code_area" => $request->code_area,
                "fax_number" => $request->fax_number,
                "email" => $request->email,
            ]);


            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Success to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
            {
                try {
                    $data = GeneralInfo::findOrFail($id);

                    return response()->json([
                        'data' => $data,
                        'status' => 200,
                        'information' => 'Success fetching data'
                    ], 200);
                } catch (\Exception $e) {
                    // Menangani jika data kosong atau ada kesalahan lainnya
                    return response()->json([
                        'error' => 'Data not found or there was an error',
                        'status' => 404, // Anda dapat menyesuaikan status kode sesuai kebutuhan
                        'information' => 'Error fetching data'
                    ], 404);
                }
            }

    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');

		// $bookmarkMe = DB::table('user_has_bookmarks')->where('user_id',$user)->get();
        try {
            $data = GeneralInfo::where('user_id', $user_id)->firstOrFail();

            // Data found, you can use $data here
            return response()->json([
                'data' => $data,
                'status'=> 200,
                'information' =>'Success fetching data'
                ], 200);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // Data not found, handle the exception here
            // For example, you can return a response indicating no data found
            return response()->json([
                'information' => 'Data not found',
                'status'=> 404,

            ], 404);
        }

	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = DB::table('vnd_mgm.trx_vnd_general_info')
                ->where('user_id', '=', $id)
                ->first();

            if (!$data) {
                return response()->json([
                    'success' => false,
                    "status" => 404,
                    'information' => 'Data not found'
                ], 404);
            }

            $updated = DB::table('vnd_mgm.trx_vnd_general_info')
                ->where('id', $data->id)
                ->update([
                    "user_id" => $request->user_id,
                    "business_qualifications" => $request->business_qualifications,
                    "regis_location" => $request->regis_location,
                    "vendor_name" => $request->vendor_name,
                    "type_bussines" => $request->type_bussines,
                    "npwp" => $request->npwp,
                    "npwp_address" => $request->npwp_address,
                    "street" => $request->street,
                    "country" => $request->country,
                    "province" => $request->province,
                    "city" => $request->city,
                    "district" => $request->district,
                    "village" => $request->village,
                    "postal_code" => $request->postal_code,
                    "rw" => $request->rw,
                    "rt" => $request->rt,
                    "phone_office" => $request->phone_office,
                    "code_area" => $request->code_area,
                    "fax_number" => $request->fax_number,
                    "email" => $request->email,
                ]);

            if ($updated) {
                return response()->json([
                    'success' => true,
                    "status" => 200,
                    'information' => 'Data updated successfully'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    "status" => 500,
                    'information' => 'Failed to update data'
                ], 500);
            }
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to update data'
            ], 500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
