<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StatusVendorResource;
use App\Models\StatusVendor;
use Illuminate\Http\Request;

class StatusVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    //update status approve pertab
    public function updateStatus(Request $request)
    {
        try {
            $status = StatusVendor::where('user_id', $request->input('user_id'))->first();
            if ($status) {
                $data = StatusVendor::findOrFail($status->id);
                $tanggalApprove = now();
                $data->tanggal_approve = $tanggalApprove;
                $data->fill($request->all());
            } else {
                $data = new StatusVendor();
                $tanggalApprove = now();
                $data->user_id = $request["user_id"];
                $data->tanggal_approve = $tanggalApprove;
                $data->fill($request->all());
            }
            if ($data->save()) {
                return response()->json([
                    'status' => 201,
                    'information' => $data,
                ], 200);
            } else {
                if (!$data) {
                    return response()->json([
                        'status' => 404,
                        'information' => 'data tidak ditemukan'
                    ], 404);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }
    }

    public function showUserApprove(Request $request)
    {
        $user_id = $request->input('user_id');


        try {
            $data = StatusVendor::where('user_id', $user_id)->paginate($request->limit)
                ->appends(['limit' => $request->limit, 'status']);

            return StatusVendorResource::collection($data)->additional([
                'status' => 200,
                'information' => 'Success fetching data'
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
