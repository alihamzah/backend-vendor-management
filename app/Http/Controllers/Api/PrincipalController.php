<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PrincipalResource;
use App\Models\Principal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // Import the DB class at the top of your file
use File;
class PrincipalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'user_id' => ['required'],
            'vendor_name' => ['required'],
            'product_name' => ['required'],
            'periode_date' => ['required'],
            'document' => ['required', 'file'], // Add 'file' rule to validate file upload
        ]);
        try {
            if ($request->hasFile('document')) {
                $file = $request->file('document');

                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);

                $data = Principal::create([
                    'user_id' => $request->user_id,
                    'vendor_name' => $request->vendor_name,
                    'product_name' => $request->product_name,
                    'periode_date' => $request->periode_date,
                    'document' => 'uploads/' . $filename, // Simpan path ke file dalam kolom 'document'
                ]);
            }else{
                $data = Principal::create([
                    'user_id' => $request->user_id,
                    'vendor_name' => $request->vendor_name,
                    'product_name' => $request->product_name,
                    'periode_date' => $request->periode_date,

                ]);
            }

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');


		try {
           $data = Principal::where('user_id', $user_id)->paginate($request->limit)
        ->appends(['limit' => $request->limit]);

        $updatedData = $data->map(function ($item) {
            // Ubah nilai document untuk mencakup URL lengkap
            $item->document = url($item->document);

            return $item;
        });
        $data->setCollection($updatedData);
        return PrincipalResource::collection($data)->additional([
            'status' => 200,
            'information' => 'Success fetching data'
        ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Principal $principal)
    {
        //
        try{
            if ($request->hasFile('document')) {
                $file = $request->file('document');

                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);
                File::delete(public_path($principal->document));
                $principal->update([
                    'user_id' => $request->user_id,
                    'vendor_name' => $request->vendor_name,
                    'product_name' => $request->product_name,
                    'periode_date' => $request->periode_date,
                    'document' => 'uploads/' . $filename, // Simpan path ke file dalam kolom 'document'
                ]);
            }else{

                $principal->update([
                    'user_id' => $request->user_id,
                    'vendor_name' => $request->vendor_name,
                    'product_name' => $request->product_name,
                    'periode_date' => $request->periode_date,
                ]);
            }

        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to update data'
        ], 201);
    } catch (QueryException $exception) {
        return response()->json([
            'success' => false,
            "status" => 500,
            'information' => 'Failed to update data'
        ], 500);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Principal $principal)
    {
        File::delete(public_path($principal->document));
        $principal->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
