<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PicAktaResource;
use App\Models\PicAkta;
use Illuminate\Http\Request;
use Carbon\Carbon;
use File;

class PicAktaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'user_id' => ['required'],
            'akta_type' => ['required'],
            'notaris_name' => ['required'],
            'akta_number' => ['required'],
            'date' => ['required', 'date'],
            'ahu_number' => ['required'],
            'ahu_date' => ['required', 'date'],
            'document' => ['file'],
        ]);
        // Parse and format the date strings using Carbon

        try {
            $filename = null; // Deklarasikan $filename di luar blok if
            if ($request->hasFile('document')) {

                $file = $request->file('document');
                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);
            }


            $data = PicAkta::create([
                'user_id' => $request->user_id,
                'akta_type' => $request->akta_type ?? '-',
                'notaris_name' => $request->notaris_name,
                'akta_number' => $request->akta_number,
                'date' => $request->date,
                'ahu_number' => $request->ahu_number,
                'ahu_date' => $request->ahu_date,
                'document' => $filename ?? null // Menyimpan nama file jika diunggah, atau null jika tidak ada file
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showUser(Request $request)
    {
        $user_id = $request->input('user_id');


        try {
            $data = PicAkta::where('user_id', $user_id)->paginate($request->limit)
                ->appends(['limit' => $request->limit]);

            $updatedData = $data->map(function ($item) {
                // Ubah nilai document untuk mencakup URL lengkap
                $item->document = url('uploads/' . $item->document);

                return $item;
            });
            return PicAktaResource::collection($updatedData)->additional([
                'status' => 200,
                'information' => 'Success fetching data'
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PicAkta $aktum)
    {
        //

        try {
            if ($request->hasFile('document')) {
                File::delete(public_path('uploads/' . $aktum->document));

                $filename = null; // Deklarasikan $filename di luar blok if


                $file = $request->file('document');
                $originalName = $file->getClientOriginalName();
                $filename = time() . '_' . str_replace(' ', '_', $originalName);
                $file->move(public_path('uploads'), $filename);

                $aktum->update([
                    'user_id' => $request->user_id,
                    'akta_type' => $request->akta_type ?? '-',
                    'notaris_name' => $request->notaris_name,
                    'akta_number' => $request->akta_number,
                    'date' => $request->date,
                    'ahu_number' => $request->ahu_number,
                    'ahu_date' => $request->ahu_date,
                    'document' => $filename ?? null // Menyimpan nama file jika diunggah, atau null jika tidak ada file
                ]);
            } else {
                $aktum->update([
                    'user_id' => $request->user_id,
                    'akta_type' => $request->akta_type ?? '-',
                    'notaris_name' => $request->notaris_name,
                    'akta_number' => $request->akta_number,
                    'date' => $request->date,
                    'ahu_number' => $request->ahu_number,
                    'ahu_date' => $request->ahu_date,

                ]);
            }
            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Success to update data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to update data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PicAkta $aktum)
    {
        File::delete(public_path('uploads/' . $aktum->document));
        $aktum->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
