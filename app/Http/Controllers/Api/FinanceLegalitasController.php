<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FinanceLegalitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // Import the DB class at the top of your file
use Illuminate\Support\Facades\Storage;
use File;
class FinanceLegalitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'user_id' => ['required', 'unique:trx_legal_finance'],
            'periode_tahun_buku' => ['required'],
            'jumlah_karyawan' => ['required'],

            'nomor_bpkm' => ['required'],
            'periode_bpkm' => ['required'],
            'masa_berlaku_bpkm' => ['required'],
            'document_bpkm' => ['required'],

            'nomor_nib' => ['required'],
            'tanggal_nib' => ['required'],
            'masa_berlaku_nib' => ['required'],
            'document_nib' => ['required'],

            'nomor_domisili' => ['required'],
            'tanggal_domisili' => ['required'],
            'masa_berlaku_domisili' => ['required'],
            'document_domisili' => ['required'],

            'nomor_npwp' => ['required'],
            'alamat_npwp' => ['required'],
            'document_npwp' => ['required'],

            'status_pajak' => ['required'],
            'masa_berlaku_pajak' => ['required'],
            'aset_lancar' => ['required'],
            'kewajiban_jangka_pendek' => ['required'],
            'document_pajak' => ['required'],

            'bukti_lapor_pph' => ['required'],
            'bukti_lapor_spt' => ['required'],
            'document_keuangan' => ['required'],

        ]);
         // Parse and format the date strings using Carbon

         try {

            $documentFields = [
                'document_bpkm', 'document_nib',
                'document_domisili', 'document_npwp', 'document_pajak',
                'bukti_lapor_pph', 'bukti_lapor_spt', 'document_keuangan'
            ];

            $documentData = [];

            foreach ($documentFields as $field) {
                if ($request->hasFile($field)) {
                    $file = $request->file($field);
                    $originalName = $file->getClientOriginalName();
                    $newDocumentName = time() . '_' . str_replace(' ', '_', $originalName);
                    $file->move(public_path('uploads'), $newDocumentName);
                    $documentData[$field] = $newDocumentName;
                }
            }

            $data = [
                'user_id' => $request->user_id,
                'periode_tahun_buku' => $request->periode_tahun_buku ?? '-',
                'jumlah_karyawan' => $request->jumlah_karyawan,

                'nomor_bpkm' => $request->nomor_bpkm,
                'periode_bpkm' => $request->periode_bpkm,
                'masa_berlaku_bpkm' => $request->masa_berlaku_bpkm,

                'nomor_nib' => $request->nomor_nib,
                'tanggal_nib' =>  $request->tanggal_nib,
                'masa_berlaku_nib' =>  $request->masa_berlaku_nib,

                'nomor_domisili' =>  $request->nomor_domisili,
                'tanggal_domisili' =>  $request->tanggal_domisili,
                'masa_berlaku_domisili' =>  $request->masa_berlaku_domisili,

                'nomor_npwp' =>  $request->nomor_npwp,
                'alamat_npwp' =>  $request->alamat_npwp,
                'masa_berlaku_domisili' =>  $request->masa_berlaku_domisili,


                'status_pajak' =>  $request->status_pajak,
                'masa_berlaku_pajak' =>  $request->masa_berlaku_pajak,
                'aset_lancar' =>  $request->aset_lancar,
                'kewajiban_jangka_pendek' =>  $request->kewajiban_jangka_pendek,
            ];

            $data = array_merge($data, $documentData);
            FinanceLegalitas::create($data);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');

		// $bookmarkMe = DB::table('user_has_bookmarks')->where('user_id',$user)->get();
        try {


            $data = FinanceLegalitas::where('user_id', $user_id)->firstOrFail();
            $data->document_bpkm = url('uploads/' . $data->document_bpkm);
            $data->document_nib = url('uploads/' . $data->document_nib);
            $data->document_domisili = url('uploads/' . $data->document_domisili);
            $data->document_npwp = url('uploads/' . $data->document_npwp);
            $data->document_pajak = url('uploads/' . $data->document_pajak);
            $data->bukti_lapor_pph = url('uploads/' . $data->bukti_lapor_pph);
            $data->bukti_lapor_spt = url('uploads/' . $data->bukti_lapor_spt);
            $data->document_keuangan = url('uploads/' . $data->document_keuangan);


            $response = [
                    "id" => $data->id,
                    "status" => $data->status,
                    "legal" => [
                        "tahun_buku_perusahaan" => [
                            "periode" => $data->periode_tahun_buku,
                            "jumlah_karyawan" => $data->jumlah_karyawan,
                        ],
                        "ijin_bpkm" => [
                            "nomor_bpkm" => $data->nomor_bpkm,
                            "periode_bpkm" => $data->periode_bpkm,
                            "masa_berlaku_bpkm" => $data->masa_berlaku_bpkm,
                            "document_bpkm" => $data->document_bpkm,
                        ],
                        "nib" => [
                            "nomor_nib" => $data->nomor_nib,
                            "tanggal_nib" => $data->tanggal_nib,
                            "masa_berlaku_nib" => $data->masa_berlaku_nib,
                            "document_nib" => $data->document_nib,
                        ],
                        "keterangan_domisili" => [
                            "nomor_domisili" => $data->nomor_domisili,
                            "tanggal_domisili" => $data->tanggal_domisili,
                            "masa_berlaku_domisili" => $data->tanggal_domisili,
                            "document_domisili" => $data->document_domisili,
                        ],
                    ],
                    "finance" => [
                        "npwp" => [
                            "nomor_npwp" => $data->nomor_npwp,
                            "alamat_npwp" => $data->alamat_npwp,
                            "document_npwp" => $data->document_npwp,
                        ],
                        "wajib_pajak" => [
                            "status_pajak" => $data->status_pajak,
                            "file_pajak" => $data->document_pajak,
                            "masa_berlaku_pajak" => $data->masa_berlaku_pajak,
                        ],
                        "bukti_lapor_pph" => $data->bukti_lapor_pph,
                        "bukti_lapor_spt" => $data->bukti_lapor_spt,
                        "laporan_keuangan_terakhir" => [
                            "aset_lancar" => $data->aset_lancar,
                            "kewajiban_jangka_pendek" => $data->kewajiban_jangka_pendek,
                            "document_keuangan" => $data->document_keuangan,
                        ],
                    ],
            ];
            // Data found, you can use $data here
            return response()->json([
                'data' => $response,
                'status'=> 200,
                'information' =>'Success fetching data'
                ], 200);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // Data not found, handle the exception here
            // For example, you can return a response indicating no data found
            return response()->json([
                'information' => 'Data not found',
                'status'=> 404,

            ], 404);
        }

	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceLegalitas $legal_finance)
    {


        $documentFields = [
            'document_bpkm', 'document_nib',
            'document_domisili', 'document_npwp', 'document_pajak',
            'bukti_lapor_pph', 'bukti_lapor_spt', 'document_keuangan'
        ];
                  foreach ($documentFields as $field) {

                    if ($request->hasFile($field)) {

                        // Hapus dokumen lama jika ada
                        if (!empty($legal_finance->$field)) {

                            File::delete(public_path('uploads/'.$legal_finance->$field));
                        }

                        // Upload dokumen baru
                        $file = $request->file($field);
                        $originalName = $file->getClientOriginalName();
                        $newDocumentName = time() . '_' . str_replace(' ', '_', $originalName);
                        $file->move(public_path('uploads'), $newDocumentName);

                        // Update field dokumen di model
                        $legal_finance->$field = $newDocumentName;
                    }
                     }

                    $legal_finance->update([
                        'user_id' => $request->user_id,
                'periode_tahun_buku' => $request->periode ?? '-',
                'jumlah_karyawan' => $request->jumlah_karyawan,

                'nomor_bpkm' => $request->nomor_bpkm,
                'periode_bpkm' => $request->periode_bpkm,
                'masa_berlaku_bpkm' => $request->masa_berlaku_bpkm,

                'nomor_nib' => $request->nomor_nib,
                'tanggal_nib' =>  $request->tanggal_nib,
                'masa_berlaku_nib' =>  $request->masa_berlaku_nib,

                'nomor_domisili' =>  $request->nomor_domisili,
                'tanggal_domisili' =>  $request->tanggal_domisili,
                'masa_berlaku_domisili' =>  $request->masa_berlaku_domisili,

                'nomor_npwp' =>  $request->nomor_npwp,
                'alamat_npwp' =>  $request->alamat_npwp,
                'masa_berlaku_domisili' =>  $request->masa_berlaku_domisili,


                'status_pajak' =>  $request->status_pajak,
                'masa_berlaku_pajak' =>  $request->masa_berlaku_pajak,
                'aset_lancar' =>  $request->aset_lancar,
                'kewajiban_jangka_pendek' =>  $request->kewajiban_jangka_pendek,
                    ]);
                    return response()->json([
                        'success' => true,
                        "status" => 201,
                        'information' => 'Succes to Update data'
                    ], 201);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
