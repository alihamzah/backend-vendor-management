<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PicVendor;
use App\Http\Resources\PicVendorResource;

class PicVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required'],
            'pic_name' => ['required'],
            'pic_position' => ['required'],
            'no_identity' => ['required'],
            'periode_time' => ['required'],
            'address' => ['required'],
            'npwp' => ['required'],
            'email' => ['required', 'email'],
            'phone' => ['required'],
        ]);
        try {
            $data = PicVendor::create([
                "user_id" => $request->user_id,
                "pic_name" => $request->pic_name,
                "pic_position" => $request->pic_position,
                "no_identity" => $request->no_identity,
                "periode_time" => $request->periode_time,
                "address" => $request->address,
                "npwp" => $request->npwp,
                "email" => $request->email,
                "phone" => $request->phone,
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Success to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
    {

        try {
            $user_id = $request->input('user_id');

            $data = PicVendor::where('user_id', $user_id)->paginate($request->limit)
                ->appends(['limit' => $request->limit]);

            return PicVendorResource::collection($data)->additional([
                'status' => 200,
                'information' => 'Success fetching data'
            ]);

            return PicVendorResource::collection($data)->additional([
                'status' => 200,
                'information' => 'Success fetching data'
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PicVendor $pic)
    {
        //
        try{
        $pic->update([
                "user_id" => $request->user_id,
                "pic_name" => $request->pic_name,
                "pic_position" => $request->pic_position,
                "no_identity" => $request->no_identity,
                "periode_time" => $request->periode_time,
                "address" => $request->address,
                "npwp" => $request->npwp,
                "email" => $request->email,
                "phone" => $request->phone,
        ]);
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to update data'
        ], 201);
    } catch (QueryException $exception) {
        return response()->json([
            'success' => false,
            "status" => 500,
            'information' => 'Failed to update data'
        ], 500);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PicVendor $pic)
    {
        $pic->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
