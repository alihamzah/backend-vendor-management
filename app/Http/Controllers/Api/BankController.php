<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BankResource;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required'],
            'rekening_holder' => ['required'],
            'rekening_number' => ['required'],
            'currency' => ['required'],
            'bank_name' => ['required'],
            'bank_chapter' => ['required'],
            'clearing_code' => ['required'],
            'city' => ['required'],
            'primary' => ['required'],
        ]);
        try {
            $data = Bank::create([
                'user_id' => $request->user_id,
                'rekening_holder' => $request->rekening_holder,
                'rekening_number' => $request->rekening_number,
                'currency' => $request->currency,
                'bank_name' => $request->bank_name,
                'bank_chapter' => $request->bank_chapter,
                'clearing_code' => $request->clearing_code,
                'city' => $request->city,
                'primary' => $request->primary,
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');


		try {
           $data = Bank::where('user_id', $user_id)->paginate($request->limit)
        ->appends(['limit' => $request->limit, 'status']);

        return BankResource::collection($data)->additional([
            'status' => 200,
            'information' => 'Success fetching data'
        ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        //
        try{
        $bank->update([
            'user_id' => $request->user_id,
                'rekening_holder' => $request->rekening_holder,
                'rekening_number' => $request->rekening_number,
                'currency' => $request->currency,
                'bank_name' => $request->bank_name,
                'bank_chapter' => $request->bank_chapter,
                'clearing_code' => $request->clearing_code,
                'city' => $request->city,
                'primary' => $request->primary,
        ]);
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to update data'
        ], 201);
    } catch (QueryException $exception) {
        return response()->json([
            'success' => false,
            "status" => 500,
            'information' => 'Failed to update data'
        ], 500);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
