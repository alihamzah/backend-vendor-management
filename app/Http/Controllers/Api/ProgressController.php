<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Afiliasi;
use App\Models\Bank;
use App\Models\DocPendukung;
use App\Models\FinanceLegalitas;
use App\Models\GeneralInfo;
use App\Models\Kompetensi;
use App\Models\PicAkta;
use App\Models\PicContact;
use App\Models\PicKepemilikan;
use App\Models\PicVendor;
use App\Models\Principal;
use App\Models\SignContract;
use Illuminate\Http\Request;

class ProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function showSummary(Request $request){
         $user_id = $request->input('user_id');
            try {

                $data = GeneralInfo::where('user_id', $user_id)->count();
                $pdata = PicVendor::where('user_id', $user_id)->count();
                $bdata = Bank::where('user_id', $user_id)->count();
                $afiliasdata = Afiliasi::where('user_id', $user_id)->count();
                $komdata = Kompetensi::where('user_id', $user_id)->count();
                $legdata = FinanceLegalitas::where('user_id', $user_id)->count();


                $response =[
                    "user" => 10,
                    "data_perusahaan" =>  $data > 0 ?  12.86 : 0,
                    "penanggung_jawab" => $pdata > 0 ? 12.86: 0,
                    "contact_person" => $pdata > 0 ? 12.86: 0,
                    "bank" => $bdata ? 12.86 : 0,
                    "afiliasi" => $afiliasdata > 0 ? 12.86 : 0,
                    "kompetensi_referensi" =>$komdata ? 12.86 : 0,
                    "finance_legal" => $legdata ? 12.86 : 0,


                ];
                $total_progress = 0;

            // Menyederhanakan perhitungan total_progress dengan loop
            foreach ($response as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $subKey => $subValue) {
                        $total_progress += floatval($subValue);
                    }
                } else {
                    $total_progress += floatval($value);
                }
            }

             $response["total_progress"] = floatval(number_format($total_progress, 2));

                // Data found, you can use $data here
                return response()->json([
                    'data' => $response,
                    'status'=> 200,
                    'information' =>'Success fetching data'
                    ], 200);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                // Data not found, handle the exception here
                // For example, you can return a response indicating no data found
                return response()->json([
                    'information' => 'Data not found',
                    'status'=> 404,

                ], 404);
            }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
