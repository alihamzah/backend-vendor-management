<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PicContacResource;
use App\Models\PicContact;
use Illuminate\Http\Request;

class PicContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'user_id' => ['required'],
            'pic_name' => ['required'],
            'pic_position' => ['required'],
            'phone' => ['required'],
            'type' => ['required'],
            'email' => ['required', 'email'],
        ]);
        try {
            $data = PicContact::create([
                'user_id' => $request->user_id,
                'pic_position' => $request->pic_position ?? '-',
                'pic_name' => $request->pic_name,
                'phone' => $request->phone,
                'type' => $request->type,
                'email' => $request->email
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');
        $type = $request->input('type');




		try {
            $data= null;
            if(!empty($type)){

           $data = PicContact::where('user_id', $user_id)->where('type',$type)->paginate($request->limit)
        ->appends(['limit' => $request->limit]);
            }else{
                $data = PicContact::where('user_id', $user_id)->paginate($request->limit)
                ->appends(['limit' => $request->limit]);
            }

        return PicContacResource::collection($data)->additional([
            'status' => 200,
            'information' => 'Success fetching data'
        ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $account_manager = PicContact::findOrFail($id);

            $account_manager->update([
                'user_id' => $request->user_id,
                'pic_position' => $request->pic_position ?? '-',
                'pic_name' => $request->pic_name,
                'phone' => $request->phone,
                'type' => $request->type,
                'email' => $request->email
            ]);

            return response()->json([
                'success' => true,
                'status' => 201,
                'information' => 'Success to update data'
            ], 201);
        } catch (ModelNotFoundException $notFoundException) {
            return response()->json([
                'success' => false,
                'status' => 404,
                'information' => 'Data not found'
            ], 404);
        } catch (QueryException $queryException) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'information' => 'Failed to update data'
            ], 500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{
    try {
        $account_manager = PicContact::findOrFail($id);
        $account_manager->delete();

        return response()->json([
            'success' => true,
            'status' => 200,
            'information' => 'Data deleted successfully'
        ], 200);
    } catch (ModelNotFoundException $notFoundException) {
        return response()->json([
            'success' => false,
            'status' => 404,
            'information' => 'Data not found'
        ], 404);
    } catch (\Exception $exception) {
        return response()->json([
            'success' => false,
            'status' => 500,
            'information' => 'Failed to delete data'
        ], 500);
    }
}

}
