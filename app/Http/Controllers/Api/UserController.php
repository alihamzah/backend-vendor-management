<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Afiliasi;
use App\Models\Bank;
use App\Models\FinanceLegalitas;
use App\Models\GeneralInfo;
use App\Models\Kompetensi;
use App\Models\PicContact;
use App\Models\PicKepemilikan;
use App\Models\PicVendor;
use App\Models\SignContract;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function updateUser(Request $request)
    {
        try {
        $user = User::find($request->input('user_id'));


        if (!$user) {
            return response()->json([
                'status' => 404,
                'information' => 'data tidak ditemukan'
            ], 404);
        }
        $user->fill($request->all());
        $user->save();

         return response()->json([

            'status' => 201,
            'information' => 'Success update'
        ], 200);
     }catch (\Exception $e) {
        return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
    }
    }
    public function updateUserStatus(Request $request)
    {
        try {

        $user = User::find($request->input('user_id'));

        if (!$user) {
            return response()->json([
                'status' => 404,
                'information' => 'data tidak ditemukan'
            ], 404);
        }
        $user->fill($request->all());
        $user->save();

         return response()->json([

            'status' => 201,
            'information' => 'Success update'
        ], 200);
     }catch (\Exception $e) {
        return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
    }
    }
    //update status general info
    public function updateGeneralInfoStatus(Request $request)
    {
        try {


          $generalInfo = GeneralInfo::where('user_id', $request->input('user_id'))->first();


        if (!$generalInfo) {
            return response()->json([
                'status' => 404,
                'information' => 'data tidak ditemukan'
            ], 404);
        }
        $generalInfo->fill($request->all());
        $generalInfo->save();

         return response()->json([

            'status' => 201,
            'information' => 'Success update'
        ], 200);
     }catch (\Exception $e) {
        return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
    }
    }
     //update status Penanggung Jawab
     public function updatePICStatus(Request $request)
     {
         try {


           $picVendor = PicVendor::where('user_id', $request->input('user_id'))->first();
           $signContract = SignContract::where('user_id', $request->input('user_id'))->first();
           $picKepemilikan = PicKepemilikan::where('user_id', $request->input('user_id'))->first();


         if (!$picVendor) {
             return response()->json([
                 'status' => 404,
                 'information' => 'data tidak ditemukan'
             ], 404);
         }
         $picVendor->fill($request->all());
         $picVendor->save();

        //
        $signContract->fill($request->all());
        $signContract->save();
        //
        $picKepemilikan->fill($request->all());
        $picKepemilikan->save();

          return response()->json([

             'status' => 201,
             'information' => 'Success update'
         ], 200);
      }catch (\Exception $e) {
         return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
     }
     }
      //update status Contact and Finance
      public function updateContactStatus(Request $request)
      {
          try {


            $contact = PicContact::where('user_id', $request->input('user_id'))->first();



          if (!$contact) {
              return response()->json([
                  'status' => 404,
                  'information' => 'data tidak ditemukan'
              ], 404);
          }
          $contact->fill($request->all());
          $contact->save();

           return response()->json([

              'status' => 201,
              'information' => 'Success update'
          ], 200);
       }catch (\Exception $e) {
          return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
      }
      }
      //update status bank
      public function updateBankStatus(Request $request)
      {
          try {


            $contact = Bank::where('user_id', $request->input('user_id'))->first();



          if (!$contact) {
              return response()->json([
                  'status' => 404,
                  'information' => 'data tidak ditemukan'
              ], 404);
          }
          $contact->fill($request->all());
          $contact->save();

           return response()->json([

              'status' => 201,
              'information' => 'Success update'
          ], 200);
       }catch (\Exception $e) {
          return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
      }
      }
       //update status Afiliasi
       public function updateAfiliasiStatus(Request $request)
       {
           try {


             $afiliasi = Afiliasi::where('user_id', $request->input('user_id'))->first();



           if (!$afiliasi) {
               return response()->json([
                   'status' => 404,
                   'information' => 'data tidak ditemukan'
               ], 404);
           }
           $afiliasi->fill($request->all());
           $afiliasi->save();

            return response()->json([

               'status' => 201,
               'information' => 'Success update'
           ], 200);
        }catch (\Exception $e) {
           return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
       }
       }
        //update status komref
        public function updateKomRefStatus(Request $request)
        {
            try {


              $komRef = Kompetensi::where('user_id', $request->input('user_id'))->first();



            if (!$komRef) {
                return response()->json([
                    'status' => 404,
                    'information' => 'data tidak ditemukan'
                ], 404);
            }
            $komRef->fill($request->all());
            $komRef->save();

             return response()->json([

                'status' => 201,
                'information' => 'Success update'
            ], 200);
         }catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }
        }
         //update status financeLegalitas
         public function updateFinanceLegalitasStatus(Request $request)
         {
             try {


               $komRef = FinanceLegalitas::where('user_id', $request->input('user_id'))->first();



             if (!$komRef) {
                 return response()->json([
                     'status' => 404,
                     'information' => 'data tidak ditemukan'
                 ], 404);
             }
             $komRef->fill($request->all());
             $komRef->save();

              return response()->json([

                 'status' => 201,
                 'information' => 'Success update'
             ], 200);
          }catch (\Exception $e) {
             return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
         }
         }
    public function showUser(Request $request)
	{

        try {
            $user_id = $request->input('user_id');

        $data = User::leftjoin('mst_vnd_type', 'users.vendor_type_id', '=', 'mst_vnd_type.id')
        ->leftjoin('mst_sub_vnd_type', 'users.sub_type_vendor_id', '=', 'mst_sub_vnd_type.id')
        ->select('users.*', 'mst_vnd_type.vendor_type','mst_sub_vnd_type.sub_type_vendor')
        ->where('users.id', $user_id)
        ->first();

        if (!$data) {
            return response()->json([
                'status' => 404,
                'information' => 'data tidak ditemukan'
            ], 404);
        }
        $token = $data->createToken($data->email);

        return response()->json([
            'data' => $data,
            'status' => 200,
            'token'=> $token->plainTextToken,
            'information' => 'Success login'
        ], 200);


            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
            }
	}
    public function showAllUser(Request $request)
	{

        try {
        $data = User::leftjoin('mst_vnd_type', 'users.vendor_type_id', '=', 'mst_vnd_type.id')
        ->leftjoin('mst_sub_vnd_type', 'users.sub_type_vendor_id', '=', 'mst_sub_vnd_type.id')
        ->select('users.*', 'mst_vnd_type.vendor_type','mst_sub_vnd_type.sub_type_vendor')->get();

        if (!$data) {
            return response()->json([
                'status' => 401,
                'information' => 'Failed fetching data all user'
            ], 401);
        }
        // $token = $data->createToken($data->email);

        return UserResource::collection($data)->additional([
            'status' => 200,
            // 'token'=> $token->plainTextToken,
            'information' => 'Success fetching data all user'
        ]);

            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
            }
	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
