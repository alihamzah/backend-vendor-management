<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PicKepemilikanResource;
use App\Http\Resources\PicVendorResource;
use App\Models\PicKepemilikan;
use Illuminate\Http\Request;

class PicKepemilikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PicKepemilikan::all();

		return PicKepemilikanResource::collection($data)->additional([

            "status"=> 200,
            "information"=> "Success fetching data",
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required'],
            'pic_name' => ['required'],
            'ownership' => ['required'],
            'pic_position' => ['required'],
        ]);
        try {
            $data = PicKepemilikan::create([
                'user_id' => $request->user_id,
                'designation' => $request->designation ?? '-',
                'pic_name' => $request->pic_name,
                'ownership' => $request->ownership,
                'pic_position' => $request->pic_position
            ]);

            return response()->json([
                'success' => true,
                "status" => 201,
                'information' => 'Succes to insert data'
            ], 201);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                "status" => 500,
                'information' => 'Failed to insert data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showUser(Request $request)
	{
        $user_id = $request->input('user_id');


		try {
           $data = PicKepemilikan::where('user_id', $user_id)->paginate($request->limit)
        ->appends(['limit' => $request->limit]);

        return PicVendorResource::collection($data)->additional([
            'status' => 200,
            'information' => 'Success fetching data'
        ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'information' => 'An error occurred'], 500);
        }

	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PicKepemilikan $kepemilikan)
    {
        //
        try{
        $kepemilikan->update([
            'user_id' => $request->user_id,
            'designation' => $request->designation ?? '-',
            'pic_name' => $request->pic_name,
            'ownership' => $request->ownership,
            'pic_position' => $request->pic_position
        ]);
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to update data'
        ], 201);
    } catch (QueryException $exception) {
        return response()->json([
            'success' => false,
            "status" => 500,
            'information' => 'Failed to update data'
        ], 500);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PicKepemilikan $kepemilikan)
    {
        $kepemilikan->delete();
        return response()->json([
            'success' => true,
            "status" => 201,
            'information' => 'Success to Deleted data'
        ], 201);
    }
}
