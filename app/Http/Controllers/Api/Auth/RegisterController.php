<?php

namespace App\Http\Controllers\Api\Auth;

use App\Actions\Fortify\PasswordValidationRules;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Mail\KCIMailTest;
use App\Models\StatusVendor;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'vendor_type_id' => [''],
            'sub_type_vendor_id' => [''],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique(User::class),],
            'password' => ['required','confirmed'],
            'npwp' => [''],
        ]);


        $user = User::create([
            'vendor_type_id' => $request->vendor_type_id,
            'sub_type_vendor_id' => $request->sub_type_vendor_id,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'npwp' => $request->npwp,
            'profesi' => $request->profesi,
            'role' => $request->role,
            'status' => $request->status,
            'kewarganegaraan' => $request->kewarganegaraan,
        ]);

        //status vednor create
        $vendorSatus = StatusVendor::create([
            'user_id' => $user->id,
            'status_general_info' => 'waiting_approval',
            'status_penanggung_jawab' => 'waiting_approval',
            'status_pic' => 'waiting_approval',
            'status_bank' => 'waiting_approval',
            'status_affiliasi' => 'waiting_approval',
            'status_kompetensi' => 'waiting_approval',
            'status_legal_finance' => 'waiting_approval',

        ]);
        // $this->kirim_email();
        $token = $user->createToken($request->email);

        return (new UserResource($user))->additional([
            'token' => $token->plainTextToken,
            'information' => 'Success register',
            'status' =>200
        ]);
    }
    public function kirim_email()
    {
        Mail::to("danihidayat015@gmail.com")->send(new KCIMailTest());
        return "Email telah dikirim";
    }
}
