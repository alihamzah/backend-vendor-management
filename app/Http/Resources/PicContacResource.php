<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PicContacResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'user_id' => $this->user_id,
            'name' => $this->pic_name, // Diganti dengan pic_name dari data awal
            'position' => $this->pic_position, // Diganti dengan pic_position dari data awal
            'identity' => 'doc a',
            'type' => $this->type,
            'identity_number' => $this->phone, // Diganti dengan phone dari data awal
            'email' => $this->email,
        ];
    }
}
