<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GeneralInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'regis_location' => $this->regis_location,
            'vendor_name' => $this->vendor_name,
            'type_bussines' => $this->type_bussines,
            'npwp' => $this->npwp,
            'npwp_address' => $this->npwp_address,
            'address'=> [
                'country' => $this->country,
                'province' => $this->province,
                'city' => $this->city,
                'district' => $this->district,
                'village' => $this->village,
                'street' => $this->street,
                'rt' => $this->rt,
                'rw' => $this->rw,
            ],
            'postal_code' => $this->postal_code,
            'phone_office' => $this->phone_office,
            'code_area' => $this->code_area,
            'fax_number' => $this->fax_number,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'business_qualifications' => $this->business_qualifications,

        ];
    }
}
