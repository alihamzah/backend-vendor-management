<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxAktaVndTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('vnd_mgm.trx_akta_vnd', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->uuid('user_id');
    //         $table->string('akta_type', 100)->nullable();
    //         $table->string('notaris_name')->nullable();
    //         $table->string('akta_number', 100)->nullable();
    //         $table->date('date')->nullable();
    //         $table->string('ahu_number', 150)->nullable();
    //         $table->date('ahu_date')->nullable();
    //         $table->string('document')->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vnd_mgm.trx_akta_vnd');
    }
}
