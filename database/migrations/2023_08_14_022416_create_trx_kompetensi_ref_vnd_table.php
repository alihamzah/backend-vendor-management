<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxKompetensiRefVndTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('trx_kompetensi_ref_vnd', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->bigInteger('user_id');
    //         $table->string('competence_code')->nullable();
    //         $table->string('competence')->nullable();
    //         $table->string('experience')->nullable();
    //         $table->string('status', 100)->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_kompetensi_ref_vnd');
    }
}
