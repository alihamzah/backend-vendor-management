<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxSignContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('vnd_mgm.trx_sign_contract_vnd', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->uuid('user_id');
    //         $table->string('pic_position')->nullable();
    //         $table->string('pic_name')->nullable();
    //         $table->string('email', 100)->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vnd_mgm.trx_sign_contract_vnd');
    }
}
