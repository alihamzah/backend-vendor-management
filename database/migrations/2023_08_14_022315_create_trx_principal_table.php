<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxPrincipalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('trx_principal', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->bigInteger('user_id');
    //         $table->string('vendor_name')->nullable();
    //         $table->string('product_name')->nullable();
    //         $table->string('periode_date')->nullable();
    //         $table->string('document')->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_principal');
    }
}
