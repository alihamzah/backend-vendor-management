<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxKepemilikanVndTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('vnd_mgm.trx_kepemilikan_vnd', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->uuid('user_id');
    //         $table->string('designation')->nullable();
    //         $table->string('pic_name')->nullable();
                // $table->string('pic_position')->nullable();
    //         $table->string('ownership', 100)->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vnd_mgm.trx_kepemilikan_vnd');
    }
}
