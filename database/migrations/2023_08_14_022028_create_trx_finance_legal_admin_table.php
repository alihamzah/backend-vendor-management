<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxFinanceLegalAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('trx_finance_legal_admin', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->bigInteger('user_id');

    //         $table->string('number_identity')->nullable();
    //         $table->date('periode_identity')->nullable();
    //         $table->string('document_identity')->nullable();

    //         $table->string('number_bpkm')->nullable();
    //         $table->date('periode_bpkm')->nullable();
    //         $table->string('document_bpkm')->nullable();

    //         $table->string('number_domisili')->nullable();
    //         $table->text('alamat_domisili')->nullable();
    //         $table->string('document_domisili')->nullable();

    //         $table->string('number_npwp')->nullable();
    //         $table->string('address_npwp')->nullable();
    //         $table->string('document_npwp')->nullable();

    //         $table->string('status_wajib_pajak')->nullable();
    //         $table->string('document_wajib_pajak')->nullable();
    //         $table->date('periode_date_wajib_pajak')->nullable();

    //         $table->string('document_pph_wajib_pajak')->nullable();

    //         $table->string('document_vnd_profile')->nullable();
    //         $table->string('number_vnd_profile')->nullable();
    //         $table->string('about_vnd_profile')->nullable();

    //         $table->string('document_spt_vnd_profile')->nullable();

    //         $table->string('number_keagenan')->nullable();
    //         $table->date('periode_date_keagenan')->nullable();
    //         $table->string('document_keagenan')->nullable();

    //         $table->string('document_struktur_jabatan_keagenan')->nullable();
    //         $table->string('document_daftar_peralatan_keagenan')->nullable();

    //         $table->string('aset_lancar_report_keu')->nullable();
    //         $table->string('wajib_jangka_pendek_keu')->nullable();
    //         $table->string('document_keu')->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_finance_legal_admin');
    }
}
