<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxDocPendukungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('trx_doc_pendukung', function (Blueprint $table) {
    //         $table->uuid('id');
    //         $table->bigInteger('user_id');
    //         $table->string('letters')->nullable();
    //         $table->string('number')->nullable();
    //         $table->string('periode_date')->nullable();
    //         $table->string('document')->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_doc_pendukung');
    }
}
