<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateMstTenderOpportunities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vnd_mgm.mst_tender_opportunities', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('vendor_name')->nullable();
            $table->string('vendor_category')->nullable();
            $table->string('vendor_cualification')->nullable();
            $table->string('date_announce')->nullable();
            $table->date('date_close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vnd_mgm.mst_tender_opportunities');
    }
}
