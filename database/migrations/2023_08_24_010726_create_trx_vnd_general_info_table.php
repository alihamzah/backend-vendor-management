<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxVndGeneralInfoTable extends Migration
{
    // public function up()
    // {
    //     Schema::create('trx_vnd_general_info', function (Blueprint $table) {
    //         $table->uuid('id')->primary()->default(DB::raw('gen_random_uuid()'));
    //         $table->bigInteger('user_id');
    //         $table->string('regis_location', 255)->nullable();
    //         $table->string('vendor_name', 255)->nullable();
    //         $table->string('type_bussines', 100)->nullable();
    //         $table->string('npwp', 100)->nullable();
    //         $table->string('npwp_address', 255)->nullable();
    //         $table->text('street')->nullable();
    //         $table->string('country', 100)->nullable();
    //         $table->string('province', 200)->nullable();
    //         $table->string('city', 150)->nullable();
    //         $table->string('postal_code', 100)->nullable();
    //         $table->string('phone_office', 100)->nullable();
    //         $table->string('code_area', 100)->nullable();
    //         $table->string('fax_number', 100)->nullable();
    //         $table->string('email', 100)->nullable();
    //         $table->timestamps(6);
    //         $table->string('rt')->nullable();
    //         $table->string('rw')->nullable();
    //         $table->string('village')->nullable();
    //         $table->string('business_qualifications')->nullable();
    //         $table->string('district')->nullable();

    //         // Add foreign key constraints if needed
    //         // $table->foreign('user_id')->references('id')->on('users');
    //     });
    // }

    public function down()
    {
        Schema::dropIfExists('trx_vnd_general_info');
    }
}
