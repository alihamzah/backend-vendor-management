<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstVndStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vnd_mgm.mst_vnd_status', function (Blueprint $table) {
            $table->uuid('id');
            $table->bigInteger('user_id');
            $table->string('status_general_info', 100)->nullable();
            $table->string('status_penanggung_jawab', 100)->nullable();
            $table->string('status_pic', 100)->nullable();
            $table->string('status_bank', 100)->nullable();
            $table->string('status_affiliasi', 100)->nullable();
            $table->string('status_kompetensi', 100)->nullable();
            $table->string('status_legal_finance', 100)->nullable();
            $table->date('tanggal_approve')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_vnd_status');
    }
}
