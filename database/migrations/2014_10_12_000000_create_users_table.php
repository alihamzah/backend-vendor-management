<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vnd_mgm.users', function (Blueprint $table) {
            $table->id();
            $table->uuid('vendor_type_id')->nullable();
            $table->uuid('sub_type_vendor_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('npwp')->nullable();
            $table->string('password');
            $table->string('kewarganegaraan')->nullable();
            $table->string('profesi')->nullable();
            $table->string('role')->default('user');
            $table->string('status')->default('waiting approval');
            $table->text('aboutus')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
