<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxLegalFinanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_legal_finance', function (Blueprint $table) {
            $table->uuid('id');
            $table->bigInteger('user_id');
            $table->string('periode_tahun_buku', 100)->nullable();
            $table->string('jumlah_karyawan', 100)->nullable();

            $table->string('nomor_bpkm')->nullable();
            $table->string('periode_bkm', 100)->nullable();
            $table->date('masa_berlaku_bpkm')->nullable();
            $table->string('document_bpkm')->nullable();


            $table->string('nomor_nib')->nullable();
            $table->date('masa_berlaku_nib', 100)->nullable();
            $table->date('tanggal_nib')->nullable();
            $table->string('document_nib')->nullable();

            $table->string('nomor_domisili')->nullable();
            $table->date('masa_berlaku_domisili', 100)->nullable();
            $table->date('tanggal_domisili')->nullable();
            $table->string('document_domisili')->nullable();

            $table->string('nomor_npwp')->nullable();
            $table->text('alamat_npwp')->nullable();
            $table->string('document_npwp')->nullable();

            $table->string('status_pajak')->nullable();
            $table->string('document_pajak')->nullable();
            $table->date('masa_berlaku_pajak', 100)->nullable();

            $table->string('bukti_lapor_pph')->nullable();
            $table->string('bukti_lapor_spt')->nullable();

            $table->string('aset_lancar')->nullable();
            $table->string('kewajiban_jangka_pendek')->nullable();
            $table->string('document_keuangan')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_legal_finance');
    }
}
